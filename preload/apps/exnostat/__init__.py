import interrupt
import sys_ble
import time
import vibra
import display
import color
import buttons
import leds
import config

DM_ADV_TYPE_FLAGS = 0x01
DM_ADV_TYPE_16_UUID = 0x03
DM_ADV_TYPE_SERVICE_DATA = 0x16
UUID = b"\x6f\xfd"
TIMEOUT = 100

MODE_OFF = 0
MODE_ON_NEW_MAC = 1
MODE_ON_RX = 2
MODE_BOTH = 3

seen = {}
vib_mode = MODE_BOTH
led_mode = MODE_BOTH


def parse_advertisement_data(data):
    ads = {}

    l = len(data)
    p = 0
    while p < l:
        ad_len = data[p]
        p += 1
        if ad_len > 0:
            ad_type = data[p]
            ad_data = b""
            p += 1
            if ad_len > 1:
                ad_data = data[p : p + ad_len - 1]
                p += ad_len - 1
            ads[ad_type] = ad_data
    return ads


def bytes2hex(bin, sep=""):
    return sep.join(["%02x" % x for x in bin])


def process_covid_data(mac, service_data, rssi, flags):
    global vib_mode
    if vib_mode in [MODE_ON_RX, MODE_BOTH]:
        vibra.vibrate(10)

    if vib_mode in [MODE_ON_NEW_MAC, MODE_BOTH] and mac not in seen:
        vibra.vibrate(100)

    if led_mode in [MODE_ON_RX, MODE_BOTH]:
        leds.flash_rocket(0, 31, 20)

    if led_mode in [MODE_ON_NEW_MAC, MODE_BOTH] and mac not in seen:
        leds.flash_rocket(1, 31, 200)

    print(bytes2hex(mac, ":"), rssi, bytes2hex(service_data), flags)

    # try to produce a small int
    last_rx_time = time.time() - t0
    seen[mac] = [int(last_rx_time), flags]


def prune():
    global seen
    seen_pruned = {}
    now = time.time() - t0

    for mac in seen:
        if seen[mac][0] + TIMEOUT > now:
            seen_pruned[mac] = seen[mac]

    seen = seen_pruned


def process_scan_report(scan_report):
    ads = parse_advertisement_data(scan_report[0])
    mac = scan_report[4]
    mac = bytes([mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]])
    rssi = scan_report[1]

    # print(bytes2hex(mac, ':'), rssi, bytes2hex(scan_report[0]), ads)
    # According to spec there is no other service announced and the
    # service is always listed in the complete list of 16 bit services
    if DM_ADV_TYPE_16_UUID in ads:
        if ads[DM_ADV_TYPE_16_UUID] == UUID:
            if DM_ADV_TYPE_SERVICE_DATA in ads:
                flags = None
                if DM_ADV_TYPE_FLAGS in ads:
                    flags = ads[DM_ADV_TYPE_FLAGS]
                # service data contains another copy of the service UUID
                process_covid_data(mac, ads[DM_ADV_TYPE_SERVICE_DATA][2:], rssi, flags)


def ble_callback(_):
    while True:
        event = sys_ble.get_event()
        if event == sys_ble.EVENT_NONE:
            break
        if event == sys_ble.EVENT_SCAN_REPORT:
            while True:
                scan_report = sys_ble.get_scan_report()
                if scan_report == None:
                    break
                process_scan_report(scan_report)
            prune()


def show_stats():
    seen_t1 = 0
    seen_t2 = 0

    t = time.time() - t0

    t_min = t

    # Make a copy as `seen` could change in between
    # and we're not locking it
    seen_copy = seen.copy()
    for mac in seen_copy:
        info = seen_copy[mac]
        if info[1]:
            seen_t2 += 1
        else:
            seen_t1 += 1
        if info[0] < t_min:
            t_min = info[0]
    seen_total = seen_t1 + seen_t2

    window = t - t_min if len(seen_copy) > 0 else min(TIMEOUT, t - last_rx_time)

    disp.clear()
    disp.print("Last %u s:" % window, posy=0, fg=color.WHITE)
    disp.print("MACs:  %d" % seen_total, posy=40, fg=color.WHITE)
    disp.update()


last_rx_time = 0
disp = display.open()
v_old = 0
pause = 1

interrupt.set_callback(interrupt.BLE, ble_callback)
interrupt.enable_callback(interrupt.BLE)

try:
    vib_mode = int(config.get_string("exno_vib_mode"))
except:
    pass

try:
    led_mode = int(config.get_string("exno_led_mode"))
except:
    pass

disp.clear()
disp.print(" Exp Notif", posy=0, fg=color.WHITE)
disp.print("  Counter", posy=20, fg=color.WHITE)
disp.print("Vib Mode ->", posy=40, fg=color.GREEN)
disp.print("<- LED Mode", posy=60, fg=color.BLUE)
disp.update()

time.sleep(3)

t0 = time.time()
sys_ble.scan_start()

while True:
    v_new = buttons.read()
    v = ~v_old & v_new
    v_old = v_new

    if v & buttons.TOP_RIGHT:
        disp.clear()
        disp.print("Vib Mode:", posy=0, fg=color.WHITE)
        if vib_mode == MODE_OFF:
            vib_mode = MODE_ON_NEW_MAC
            disp.print("New MAC", posy=40, fg=color.YELLOW)
        elif vib_mode == MODE_ON_NEW_MAC:
            vib_mode = MODE_ON_RX
            disp.print("Each RX", posy=40, fg=color.BLUE)
        elif vib_mode == MODE_ON_RX:
            vib_mode = MODE_BOTH
            disp.print("MAC", posy=40, fg=color.YELLOW)
            disp.print("&", posy=40, posx=14 * 4, fg=color.WHITE)
            disp.print("RX", posy=40, posx=14 * 6, fg=color.BLUE)
        elif vib_mode == MODE_BOTH:
            vib_mode = MODE_OFF
            disp.print("Vib off", posy=40, fg=color.WHITE)
        disp.update()

        config.set_string("exno_vib_mode", str(vib_mode))
        pause = 20

    if v & buttons.BOTTOM_LEFT:
        disp.clear()
        disp.print("LED Mode:", posy=0, fg=color.WHITE)
        if led_mode == MODE_OFF:
            led_mode = MODE_ON_NEW_MAC
            disp.print("New MAC", posy=40, fg=color.YELLOW)
        elif led_mode == MODE_ON_NEW_MAC:
            led_mode = MODE_ON_RX
            disp.print("Each RX", posy=40, fg=color.BLUE)
        elif led_mode == MODE_ON_RX:
            led_mode = MODE_BOTH
            disp.print("MAC", posy=40, fg=color.YELLOW)
            disp.print("&", posy=40, posx=14 * 4, fg=color.WHITE)
            disp.print("RX", posy=40, posx=14 * 6, fg=color.BLUE)
        elif led_mode == MODE_BOTH:
            led_mode = MODE_OFF
            disp.print("LED off", posy=40, fg=color.WHITE)
        disp.update()

        config.set_string("exno_led_mode", str(led_mode))
        pause = 20

    pause -= 1
    if pause == 0:
        show_stats()
        pause = 10

    time.sleep(0.1)
