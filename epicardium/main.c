#include "modules/modules.h"
#include "os/core.h"
#include "os/work_queue.h"
#include "fs/filesystem.h"
#include "drivers/drivers.h"
#include "user_core/user_core.h"
#include "os/config.h"
#include "card10-version.h"
#include "user_core/interrupts.h"
#include "drivers/display/epic_ctx.h"

#include "leds.h"
#include "version-splash.h"

#include "FreeRTOS.h"
#include "task.h"
#include "mxc_delay.h"

#include <stdlib.h>
#include <string.h>
#include <machine/endian.h>

#include "epic_boot.c"

int main(void)
{
	watchdog_init();

	LOG_INFO("startup", "Epicardium startup ...");
	LOG_INFO("startup", "Version " CARD10_VERSION);

	LOG_DEBUG("startup", "Initializing hardware ...");
	hardware_early_init();

	load_config();

	migration_delete_app_launchers();

	//LED feedback in case of dead display
	epic_leds_set(11, 0, 0, 1);
	epic_leds_set(12, 0, 0, 1);
	epic_leds_set(13, 0, 0, 1);
	epic_leds_set(14, 0, 0, 1);

	epic_disp_clear(0x0000);

#if 0 /* reenable for future releases */
	epic_leds_set_rocket(0, 31);
	epic_leds_set_rocket(1, 31);
	epic_leds_set_rocket(2, 31);

	// TODO: Use blit function here
#if BYTE_ORDER == LITTLE_ENDIAN
	for (size_t i = 0; i < sizeof(epicardium_ctx_fb); i += 2) {
		epicardium_ctx_fb[i]     = version_splash[i + 1];
		epicardium_ctx_fb[i + 1] = version_splash[i];
	}
#else
	memcpy(epicardium_ctx_fb, version_splash, sizeof(epicardium_ctx_fb));
#endif

	if (strcmp(CARD10_VERSION, "v1.17") != 0) {
		ctx_font_size(epicardium_ctx, 20.0f);
		ctx_text_baseline(epicardium_ctx, CTX_TEXT_BASELINE_BOTTOM);
		ctx_rgba8(epicardium_ctx, 0xff, 0xc6, 0x00, 0xff);

		ctx_text_align(epicardium_ctx, CTX_TEXT_ALIGN_CENTER);
		ctx_move_to(epicardium_ctx, 80.0f, 58.0f);
		ctx_text(epicardium_ctx, "Epicardium");

		ctx_text_align(epicardium_ctx, CTX_TEXT_ALIGN_LEFT);
		ctx_move_to(epicardium_ctx, 2.0f, 78.0f);
		ctx_text(epicardium_ctx, CARD10_VERSION);

		/* re-init for the first app */
		disp_ctx_reinit();
	}
	epic_disp_update();
	mxc_delay(2000000);
#else
	for (uint32_t frame = 0; frame < 15; frame++) {
		switch (frame) {
		case 0:
			epic_leds_set_rocket(0, 31);
			break;
		case 5:
			epic_leds_set_rocket(1, 31);
			break;
		case 10:
			epic_leds_set_rocket(2, 31);
			break;
		}

		epic_disp_clear(0x0000);
		epic_frame(epicardium_ctx, frame);
		epic_disp_update();
	}

	/* re-init for the first app */
	disp_ctx_reinit();
#endif

	epic_leds_clear_all(0, 0, 0);

	LOG_DEBUG("startup", "Initializing tasks ...");

	/* Serial */
	if (xTaskCreate(
		    vSerialTask,
		    (const char *)"Serial",
		    configMINIMAL_STACK_SIZE * 2,
		    NULL,
		    tskIDLE_PRIORITY + 3,
		    NULL) != pdPASS) {
		panic("Failed to create %s task!", "Serial");
	}

	/* PMIC */
	if (xTaskCreate(
		    vPmicTask,
		    (const char *)"PMIC",
		    configMINIMAL_STACK_SIZE * 3,
		    NULL,
		    tskIDLE_PRIORITY + 4,
		    NULL) != pdPASS) {
		panic("Failed to create %s task!", "PMIC");
	}

	/* BHI160 */
	if (xTaskCreate(
		    vBhi160Task,
		    (const char *)"BHI160 Driver",
		    configMINIMAL_STACK_SIZE * 2,
		    NULL,
		    tskIDLE_PRIORITY + 1,
		    NULL) != pdPASS) {
		panic("Failed to create %s task!", "BHI160");
	}

	/* MAX30001 */
	if (xTaskCreate(
		    vMAX30001Task,
		    (const char *)"MAX30001 Driver",
		    configMINIMAL_STACK_SIZE * 2,
		    NULL,
		    tskIDLE_PRIORITY + 1,
		    NULL) != pdPASS) {
		panic("Failed to create %s task!", "MAX30001");
	}

	/* MAX86150 */
	if (xTaskCreate(
		    vMAX86150Task,
		    (const char *)"MAX86150 Driver",
		    configMINIMAL_STACK_SIZE * 2,
		    NULL,
		    tskIDLE_PRIORITY + 1,
		    NULL) != pdPASS) {
		panic("Failed to create %s task!", "MAX86150");
	}

	/* BSEC */
	if (bsec_activate() == 0) {
		if (xTaskCreate(
			    vBSECTask,
			    (const char *)"BSEC",
			    configMINIMAL_STACK_SIZE * 5,
			    NULL,
			    tskIDLE_PRIORITY + 1,
			    NULL) != pdPASS) {
			LOG_CRIT(
				"startup", "Failed to create %s task!", "BSEC"
			);
			abort();
		}
	}

	/* API */
	if (xTaskCreate(
		    vApiDispatcher,
		    (const char *)"API Dispatcher",
		    configMINIMAL_STACK_SIZE * 16,
		    NULL,
		    tskIDLE_PRIORITY + 2,
		    &dispatcher_task_id) != pdPASS) {
		panic("Failed to create %s task!", "API Dispatcher");
	}
	/* Interrupts */
	if (xTaskCreate(
		    vInterruptsTask,
		    (const char *)"Interrupt Dispatcher",
		    configMINIMAL_STACK_SIZE,
		    NULL,
		    tskIDLE_PRIORITY + 2,
		    NULL) != pdPASS) {
		panic("Failed to create %s task!", "Interrupt Dispatcher");
	}

	/* BLE */
	if (ble_is_enabled()) {
		if (xTaskCreate(
			    vBleTask,
			    (const char *)"BLE",
			    configMINIMAL_STACK_SIZE * 10,
			    NULL,
			    tskIDLE_PRIORITY + 3,
			    NULL) != pdPASS) {
			panic("Failed to create %s task!", "BLE");
		}
	}

	/* Lifecycle */
	if (xTaskCreate(
		    vLifecycleTask,
		    (const char *)"Lifecycle",
		    configMINIMAL_STACK_SIZE * 4,
		    NULL,
		    tskIDLE_PRIORITY + 3,
		    NULL) != pdPASS) {
		panic("Failed to create %s task!", "Lifecycle");
	}

	/* Work Queue */
	if (xTaskCreate(
		    vWorkQueueTask,
		    (const char *)"Work Queue",
		    configMINIMAL_STACK_SIZE * 4,
		    NULL,
		    tskIDLE_PRIORITY + 1,
		    NULL) != pdPASS) {
		panic("Failed to create %s task!", "Work Queue");
	}
	workqueue_init();

	/*
	 * Initialize serial driver data structures.
	 */
	serial_init();

	LOG_DEBUG("startup", "Starting FreeRTOS ...");
	vTaskStartScheduler();

	panic("FreeRTOS did not start due to unknown error!");
}
