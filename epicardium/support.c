/*
 * FreeRTOS support functions
 */

#include "FreeRTOS.h"
#include "task.h"

#include "api/dispatcher.h"
#include "user_core/user_core.h"
#include "os/core.h"
#include "drivers/drivers.h"
#include "modules/modules.h"

#define BB_CLK_RATE_HZ 1600000

#include "usb.h"
#include "mxc_sys.h"
#include "wut.h"
#include "wsf_types.h"
#include "wsf_os.h"

#include "sch_api_ble.h"
#include "bb_drv.h"

#include "card10.h"

#define US_TO_BBTICKS(x) (((x) * (BB_CLK_RATE_HZ / 100000) + 9) / 10)

#define MIN(a, b) (a < b) ? a : b

#define MIN_SLEEP_TIME_MS 1

static int32_t ble_sleep_ticks(void)
{
	uint32_t nextDbbEventDue;
	bool_t dueValid = SchBleGetNextDueTime(&nextDbbEventDue);
	int sleep_ticks = nextDbbEventDue - BbDrvGetCurrentTime();

	if (dueValid) {
		if (sleep_ticks > 0) {
			uint32_t bb_idle = BB_TICKS_TO_US(sleep_ticks) / 1000;
			return bb_idle;
		} else {
			return 0;
		}
	} else {
		return -1;
	}
}

/*
 * This hook is called before FreeRTOS enters tickless idle.
 */
void pre_idle_sleep(TickType_t xExpectedIdleTime)
{
	if (xExpectedIdleTime > 0 &&
	    (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) == 0) {
		/*
		 * WFE because the other core should be able to notify
		 * epicardium if it wants to issue an API call.
		 */

		/* If the other core is waiting for a call to return and we are able
		 * to sleep, reduce the system clock to save energy. */
		if (xExpectedIdleTime >= pdMS_TO_TICKS(MIN_SLEEP_TIME_MS) &&
		    api_dispatcher_call_pending() && wsfOsReadyToSleep()) {
			uint32_t ms = xExpectedIdleTime;

			if (ble_is_enabled()) {
				int32_t ble_idle = ble_sleep_ticks();
				if (ble_idle >= 0) {
					ms =
						MIN(xExpectedIdleTime,
						    (uint32_t)ble_idle);
				}
			}

			// Us the WUT only if we can sleep a significant amount of time
			if (ms >= MIN_SLEEP_TIME_MS) {
				/* Initialize Wakeup timer */
				WUT_Init(WUT_PRES_1);
				wut_cfg_t wut_cfg;
				wut_cfg.mode    = WUT_MODE_COMPARE;
				wut_cfg.cmp_cnt = 0xFFFFFFFF;
				WUT_Config(&wut_cfg);
				WUT_Enable();

				/* Enable WUT as a wakup source */
				NVIC_EnableIRQ(WUT_IRQn);

				uint32_t targetTick;
				targetTick = WUT_GetCount();
				targetTick +=
					((uint64_t)(ms)*SYS_WUT_GetFreq() /
					 1000);
				WUT_SetCompare(targetTick);

				/* Stop SysTick */
				uint32_t val = SysTick->VAL;
				SysTick->CTRL &= ~(SysTick_CTRL_ENABLE_Msk);

				if (usb_get_status() & MAXUSB_STATUS_VBUS_ON) {
					/* Need to stay on 96 MHz. USB serial becomes
					* unstable otherwise. Don't know why. */
					//SYS_Clock_Select(SYS_CLOCK_HIRC, NULL);
				} else {
					MXC_GCR->clkcn |=
						MXC_S_GCR_CLKCN_PSC_DIV4;
					SYS_Clock_Select(SYS_CLOCK_HIRC, NULL);
					SYS_ClockSourceDisable(
						SYS_CLOCK_HIRC96
					);
				}
				disp_update_backlight_clock();
				__asm volatile("dsb" ::: "memory");
				__asm volatile("wfe");
				__asm volatile("isb");
				MXC_GCR->clkcn &= ~(MXC_S_GCR_CLKCN_PSC_DIV4);
				SYS_Clock_Select(SYS_CLOCK_HIRC96, NULL);
#if 0
				/* Allow to serve high priority interrupts before
				 * doing the lengthy xTaskIncrementTick loop. */
				portDISABLE_INTERRUPTS();
				__set_PRIMASK(0);
				__set_PRIMASK(1);
				portENABLE_INTERRUPTS();
#endif
				disp_update_backlight_clock();
				SYS_ClockSourceDisable(SYS_CLOCK_HIRC);
				SysTick->LOAD = val;
				SysTick->VAL  = 0;
				SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;

				ms = (WUT_GetCount() * 1000) /
				     SYS_WUT_GetFreq();
				for (uint32_t t = 0; t < ms; t++) {
					xTaskIncrementTick();
				}
				return;
			}
		}

		/* Fall back to light sleep with clocks on if we can't
		 * sleep long enough for the WUT */
		__asm volatile("dsb" ::: "memory");
		__asm volatile("wfe");
		__asm volatile("isb");
	}
}

/*
 * This hook is called after FreeRTOS exits tickless idle.
 */
void post_idle_sleep(TickType_t xExpectedIdleTime)
{
	/* Check whether a new API call was issued. */
	if (api_dispatcher_poll_once()) {
		xTaskNotifyGive(dispatcher_task_id);
	}

	/*
	 * Do card10 house keeping. e.g. polling the i2c devices if they
	 * triggered an interrupt.
	 *
	 * TODO: Do this in a more task focused way (high/low ISR)
	 */
	card10_poll();
}

void vApplicationGetIdleTaskMemory(
	StaticTask_t **ppxIdleTaskTCBBuffer,
	StackType_t **ppxIdleTaskStackBuffer,
	uint32_t *pulIdleTaskStackSize
) {
	/*
	 * If the buffers to be provided to the Idle task are declared inside this
	 * function then they must be declared static - otherwise they will be allocated on
	 * the stack and so not exists after this function exits.
	 */
	static StaticTask_t xIdleTaskTCB;
	static StackType_t uxIdleTaskStack[configMINIMAL_STACK_SIZE];

	/*
	 * Pass out a pointer to the StaticTask_t structure in which the Idle task's
	 * ktate will be stored.
	 */
	*ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

	/* Pass out the array that will be used as the Idle task's stack. */
	*ppxIdleTaskStackBuffer = uxIdleTaskStack;

	/*
	 * Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
	 * Note that, as the array is necessarily of type StackType_t,
	 * configMINIMAL_STACK_SIZE is specified in words, not bytes.
	 */
	*pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

void vApplicationGetTimerTaskMemory(
	StaticTask_t **ppxTimerTaskTCBBuffer,
	StackType_t **ppxTimerTaskStackBuffer,
	uint32_t *pulTimerTaskStackSize
) {
	/*
	 * If the buffers to be provided to the Timer task are declared inside
	 * this function then they must be declared static - otherwise they will
	 * be allocated on the stack and so not exists after this function
	 * exits.
	 */
	static StaticTask_t xTimerTaskTCB;
	static StackType_t uxTimerTaskStack[configTIMER_TASK_STACK_DEPTH];

	/*
	 * Pass out a pointer to the StaticTask_t structure in which the Timer
	 * task's state will be stored.
	 */
	*ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

	/* Pass out the array that will be used as the Timer task's stack. */
	*ppxTimerTaskStackBuffer = uxTimerTaskStack;

	/*
	 * Pass out the size of the array pointed to by
	 * *ppxTimerTaskStackBuffer.  Note that, as the array is necessarily of
	 * type StackType_t, configMINIMAL_STACK_SIZE is specified in words, not
	 * bytes.
	 */
	*pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName)
{
	panic("Task \"%s\" overflowed stack!", pcTaskName);
}
