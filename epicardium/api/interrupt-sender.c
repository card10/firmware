#include "api/interrupt-sender.h"
#include "api/common.h"
#include "tmr_utils.h"
#include <assert.h>

void api_interrupt_init(void)
{
	API_CALL_MEM->int_id = (-1);
}

bool api_interrupt_is_ready(void)
{
	return API_CALL_MEM->int_id == (api_int_id_t)(-1);
}

void api_interrupt_trigger(api_int_id_t id)
{
	assert(API_CALL_MEM->int_id == (api_int_id_t)(-1));

	API_CALL_MEM->int_id = id;
	TMR_TO_Start(MXC_TMR5, 1, 0);
}
