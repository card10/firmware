#include "epicardium.h"
#include "drivers/display/lcd.h"
#include "drivers/display/epic_ctx.h"

#include "FreeRTOS.h"
#include "LCD_Driver.h"
#include "gpio.h"
#include "task.h"
#include "tmr.h"
#include "tmr_utils.h"

#include <machine/endian.h>
#include <string.h>

static TaskHandle_t lock = NULL;

static int check_lock()
{
	TaskHandle_t task = xTaskGetCurrentTaskHandle();
	if (task != lock) {
		return -EBUSY;
	} else {
		return 0;
	}
}

static uint16_t rgb888_to_rgb565(uint8_t *bytes)
{
	return ((bytes[0] & 0b11111000) << 8) | ((bytes[1] & 0b11111100) << 3) |
	       (bytes[2] >> 3);
}

static inline void
rgb565_to_rgb888(uint16_t pixel, uint8_t *red, uint8_t *green, uint8_t *blue)
{
	*blue  = (pixel & 31) << 3;
	*green = ((pixel >> 5) & 63) << 2;
	*red   = ((pixel >> 11) & 31) << 3;
}

int epic_disp_print(
	int16_t posx,
	int16_t posy,
	const char *pString,
	uint16_t fg,
	uint16_t bg
) {
	return epic_disp_print_adv(DISP_FONT20, posx, posy, pString, fg, bg);
}

static const float font_map[] = {
	[DISP_FONT8] = 8.0f,   [DISP_FONT12] = 12.0f, [DISP_FONT16] = 16.0f,
	[DISP_FONT20] = 20.0f, [DISP_FONT24] = 24.0f,
};

int epic_disp_print_adv(
	uint8_t font,
	int16_t posx,
	int16_t posy,
	const char *pString,
	uint16_t fg,
	uint16_t bg
) {
	uint8_t r, g, b;
	int cl = check_lock();
	if (cl < 0) {
		return cl;
	}

	if (font >= (sizeof(font_map) / sizeof(font_map[0]))) {
		return -EINVAL;
	}

	float font_size = font_map[font];
	ctx_font_size(epicardium_ctx, font_size);

	if (fg != bg) {
		/* non-transparent background */
		rgb565_to_rgb888(bg, &r, &g, &b);
		ctx_rgba8(epicardium_ctx, r, g, b, 255);
		float width = ctx_text_width(epicardium_ctx, pString);
		ctx_rectangle(epicardium_ctx, posx, posy, width, font_size);
		ctx_fill(epicardium_ctx);
	}

	rgb565_to_rgb888(fg, &r, &g, &b);
	ctx_rgba8(epicardium_ctx, r, g, b, 255);
	ctx_move_to(epicardium_ctx, posx, (float)posy + font_size * 0.8f);
	ctx_text(epicardium_ctx, pString);

	return 0;
}

int epic_disp_clear(uint16_t color)
{
	int cl = check_lock();
	if (cl < 0) {
		return cl;
	}

	/*
	 * We could use ctx for this but it's much easier to just clear the
	 * framebuffer directly.
	 */
	for (size_t i = 0; i < sizeof(epicardium_ctx_fb); i += 2) {
		epicardium_ctx_fb[i]     = color >> 8;
		epicardium_ctx_fb[i + 1] = color & 0xff;
	}

	return 0;
}

int epic_disp_pixel(int16_t x, int16_t y, uint16_t color)
{
	int cl = check_lock();
	if (cl < 0) {
		return cl;
	}

	uint8_t r, g, b;
	rgb565_to_rgb888(color, &r, &g, &b);
	ctx_set_pixel_u8(epicardium_ctx, x, y, r, g, b, 255);

	return 0;
}

static uint16_t rgb565_pixel_from_buf(
	uint8_t *img,
	enum epic_rgb_format format,
	int16_t width,
	int16_t x,
	int16_t y,
	uint8_t *alpha
) {
	uint16_t tmp16;
	uint8_t rgba[4];

	switch (format) {
	case EPIC_RGB565:
		*alpha = 255;
		memcpy(&tmp16, &img[y * width * 2 + x * 2], 2);
		return tmp16;
	case EPIC_RGBA5551:
		memcpy(&tmp16, &img[y * width * 2 + x * 2], 2);
		*alpha = (tmp16 & 0x01) ? 255 : 0;
		return (tmp16 & 0xFFC0) | ((tmp16 & 0x3E) >> 1);
	case EPIC_RGB8:
		*alpha = 255;
		memcpy(rgba, &img[y * width * 3 + x * 3], 3);
		return rgb888_to_rgb565(rgba);
	case EPIC_RGBA8:
		memcpy(rgba, &img[y * width * 4 + x * 4], 4);
		*alpha = rgba[3];
		return rgb888_to_rgb565(rgba);
	default:
		return 0xFFFF;
	}
}

int epic_disp_blit(
	int16_t pos_x,
	int16_t pos_y,
	int16_t width,
	int16_t height,
	void *img,
	enum epic_rgb_format format
) {
	int cl = check_lock();
	if (cl < 0) {
		return cl;
	}

	for (int16_t xsrc = 0; xsrc < width; xsrc += 1) {
		for (int16_t ysrc = 0; ysrc < height; ysrc += 1) {
			int16_t xscreen = pos_x + xsrc;
			int16_t yscreen = pos_y + ysrc;
			size_t offset   = yscreen * 160 * 2 + xscreen * 2;
			if (xscreen < 0 || xscreen >= 160 || yscreen < 0 ||
			    yscreen >= 80) {
				continue;
			}

			uint8_t alpha  = 255;
			uint16_t pixel = rgb565_pixel_from_buf(
				img, format, width, xsrc, ysrc, &alpha
			);

			if (alpha == 0) {
				continue;
			}

			epicardium_ctx_fb[offset]     = (pixel & 0xFF00) >> 8;
			epicardium_ctx_fb[offset + 1] = pixel & 0xFF;
		}
	}

	return 0;
}

int epic_disp_line(
	int16_t xstart,
	int16_t ystart,
	int16_t xend,
	int16_t yend,
	uint16_t color,
	enum disp_linestyle linestyle,
	uint16_t pixelsize
) {
	int cl = check_lock();
	if (cl < 0) {
		return cl;
	}

	float xstartf = xstart, ystartf = ystart, xendf = xend, yendf = yend;

	/*
	 * For odd line widths, shift the line by half a pixel so it aligns
	 * perfectly with the pixel grid.
	 */
	if (pixelsize % 2 == 1) {
		xstartf += 0.5f;
		ystartf += 0.5f;
		yendf += 0.5f;
		xendf += 0.5f;
	}

	uint8_t r, g, b;
	rgb565_to_rgb888(color, &r, &g, &b);
	ctx_rgba8_stroke(epicardium_ctx, r, g, b, 255);

	ctx_line_width(epicardium_ctx, pixelsize);
	ctx_move_to(epicardium_ctx, xstartf, ystartf);
	ctx_line_to(epicardium_ctx, xendf, yendf);
	ctx_stroke(epicardium_ctx);

	return 0;
}

int epic_disp_rect(
	int16_t xstart,
	int16_t ystart,
	int16_t xend,
	int16_t yend,
	uint16_t color,
	enum disp_fillstyle fillstyle,
	uint16_t pixelsize
) {
	int cl = check_lock();
	if (cl < 0)
		return cl;

	uint8_t r, g, b;
	rgb565_to_rgb888(color, &r, &g, &b);

	ctx_rectangle(
		epicardium_ctx,
		xstart,
		ystart,
		xend - xstart + 1,
		yend - ystart + 1
	);

	switch (fillstyle) {
	case FILLSTYLE_EMPTY:
		ctx_rgba8_stroke(epicardium_ctx, r, g, b, 255);
		ctx_line_width(epicardium_ctx, pixelsize);
		ctx_stroke(epicardium_ctx);
		break;
	case FILLSTYLE_FILLED:
		ctx_rgba8(epicardium_ctx, r, g, b, 255);
		ctx_fill(epicardium_ctx);
		break;
	}

	return 0;
}

int epic_disp_circ(
	int16_t x,
	int16_t y,
	uint16_t rad,
	uint16_t color,
	enum disp_fillstyle fillstyle,
	uint16_t pixelsize
) {
	int cl = check_lock();
	if (cl < 0)
		return cl;

	uint8_t r, g, b;
	rgb565_to_rgb888(color, &r, &g, &b);

	ctx_arc(epicardium_ctx, x, y, rad, 0.0f, CTX_PI * 1.95, 0);

	switch (fillstyle) {
	case FILLSTYLE_EMPTY:
		ctx_rgba8_stroke(epicardium_ctx, r, g, b, 255);
		ctx_line_width(epicardium_ctx, pixelsize);
		ctx_stroke(epicardium_ctx);
		break;
	case FILLSTYLE_FILLED:
		ctx_rgba8(epicardium_ctx, r, g, b, 255);
		ctx_fill(epicardium_ctx);
		break;
	}

	return 0;
}

int epic_disp_update()
{
	int cl = check_lock();
	if (cl < 0) {
		return cl;
	}

	lcd_write_fb(epicardium_ctx_fb);

	return 0;
}

int epic_disp_framebuffer(union disp_framebuffer *fb)
{
	int cl = check_lock();
	if (cl < 0) {
		return cl;
	}

	/*
	 * Flip the screen because that's what this API call historically
	 * expects.
	 */
	lcd_set_screenflip(true);
	lcd_write_fb(fb->raw);
	lcd_set_screenflip(false);
	return 0;
}

int epic_disp_backlight(uint16_t brightness)
{
	/* TODO: lock? */
	if (brightness == 0) {
		lcd_set_sleep(true);
	} else {
		lcd_set_sleep(false);
	}
	LCD_SetBacklight(brightness);
	return 0;
}

int epic_disp_open()
{
	TaskHandle_t task = xTaskGetCurrentTaskHandle();
	if (lock == task) {
		return 0;
	} else if (lock == NULL) {
		lock = task;
		return 0;
	} else {
		return -EBUSY;
	}
}

int epic_disp_close()
{
	if (check_lock() < 0 && lock != NULL) {
		return -EBUSY;
	} else {
		lock = NULL;
		return 0;
	}
}

void disp_update_backlight_clock(void)
{
	LCD_UpdateBacklightClock();
}

void disp_forcelock()
{
	TaskHandle_t task = xTaskGetCurrentTaskHandle();
	lock              = task;
}
