#include "drivers/display/lcd.h"
#include "drivers/display/epic_ctx.h"
#include "drivers/drivers.h"
#include "display.h"

#include "ctx.h"

#include <stdint.h>
#include <machine/endian.h>

#if BYTE_ORDER == LITTLE_ENDIAN
#define CARD10_CTX_FORMAT CTX_FORMAT_RGB565_BYTESWAPPED
#else
#define CARD10_CTX_FORMAT CTX_FORMAT_RGB565
#endif

uint8_t epicardium_ctx_fb[160 * 80 * 2] = { 0 };
Ctx *epicardium_ctx                     = NULL;

void disp_init(void)
{
	/*
	 * The bootloader has already initialized the display, so we only need
	 * to do the bare minimum here.
	 */
	lcd_reconfigure();

	/*
	 * Initialize the graphics context.
	 */
	disp_ctx_reinit();
}

void disp_ctx_reinit(void)
{
	if (epicardium_ctx != NULL) {
		ctx_free(epicardium_ctx);
	}

	epicardium_ctx = ctx_new_for_framebuffer(
		epicardium_ctx_fb, 160, 80, 160 * 2, CARD10_CTX_FORMAT
	);

	/* set some defaults */
	ctx_rgba(epicardium_ctx, 1.0f, 1.0f, 1.0f, 1.0f);
	ctx_rgba_stroke(epicardium_ctx, 1.0f, 1.0f, 1.0f, 1.0f);
	ctx_font(epicardium_ctx, "ctx-mono");
}
