/*************************************************************************************************/
/*!
 *  \file
 *
 *  \brief  GAP profile.
 *
 *  Copyright (c) 2015-2018 Arm Ltd. All Rights Reserved.
 *  ARM Ltd. confidential and proprietary.
 *
 *  IMPORTANT.  Your use of this file is governed by a Software License Agreement
 *  ("Agreement") that must be accepted in order to download or otherwise receive a
 *  copy of this file.  You may not use or copy this file for any purpose other than
 *  as described in the Agreement.  If you do not agree to all of the terms of the
 *  Agreement do not use this file and delete all copies in your possession or control;
 *  if you do not have a copy of the Agreement, you must contact ARM Ltd. prior
 *  to any use, copying or further distribution of this software.
 */
/*************************************************************************************************/

#include "wsf_types.h"
#include "wsf_assert.h"
#include "wsf_trace.h"
#include "app_db.h"
#include "app_api.h"
#include "gap_api.h"

#include <string.h>

/* card10:
 * copied from lib/sdk/Libraries/BTLE/stack/ble-profiles/sources/profiles/gap/gap_main.c
 */
/* clang-format off */
/* clang-formet turned off for easier diffing against orginal file */

/**************************************************************************************************
  Local Variables
**************************************************************************************************/
static char dn_value[ATT_DEFAULT_PAYLOAD_LEN + 1];

/*! GAP service characteristics for discovery */
static const attcDiscChar_t gapDn =
{
  attDnChUuid,
  0
};

/*! Central Address Resolution */
static const attcDiscChar_t gapCar =
{
  attCarChUuid,
  0
};

/*! Resolvable Private Address Only */
static const attcDiscChar_t gapRpao =
{
  attRpaoChUuid,
  0
};

/*! List of characteristics to be discovered; order matches handle index enumeration  */
static const attcDiscChar_t *gapDiscCharList[] =
{
  &gapDn,
  &gapCar,                   /* Central Address Resolution */
  &gapRpao                   /* Resolvable Private Address Only */
};

/* sanity check:  make sure handle list length matches characteristic list length */
WSF_CT_ASSERT(GAP_HDL_LIST_LEN == ((sizeof(gapDiscCharList) / sizeof(attcDiscChar_t *))));

int GapGetDeviceName(char *buf, size_t buf_size)
{
	memset(buf, 0, buf_size);
	strncpy(buf, dn_value, buf_size - 1);
	return 0;
}

void GapClearDeviceName(void)
{
	dn_value[0] = 0;
}

/*************************************************************************************************/
/*!
 *  \brief  Perform service and characteristic discovery for GAP service.  Note that pHdlList
 *          must point to an array of handles of length GAP_HDL_LIST_LEN.  If discovery is
 *          successful the handles of discovered characteristics and descriptors will be set
 *          in pHdlList.
 *
 *  \param  connId    Connection identifier.
 *  \param  pHdlList  Characteristic handle list.
 *
 *  \return None.
 */
/*************************************************************************************************/
void GapDiscover(dmConnId_t connId, uint16_t *pHdlList)
{
  AppDiscFindService(connId, ATT_16_UUID_LEN, (uint8_t *) attGapSvcUuid,
                     GAP_HDL_LIST_LEN, (attcDiscChar_t **) gapDiscCharList, pHdlList);
}

/*************************************************************************************************/
/*!
 *  \brief  Process a value received in an ATT read response, notification, or indication
 *          message.  Parameter pHdlList must point to an array of length GAP_HDL_LIST_LEN.
 *          If the attribute handle of the message matches a handle in the handle list the value
 *          is processed, otherwise it is ignored.
 *
 *  \param  pHdlList  Characteristic handle list.
 *  \param  pMsg      ATT callback message.
 *
 *  \return ATT_SUCCESS if handle is found, ATT_ERR_NOT_FOUND otherwise.
 */
/*************************************************************************************************/
uint8_t GapValueUpdate(uint16_t *pHdlList, attEvt_t *pMsg)
{
  uint8_t status = ATT_SUCCESS;

  /* device name string */
  if (pMsg->handle == pHdlList[GAP_DN_HDL_IDX])
  {
    int len = (pMsg->valueLen < (sizeof(dn_value) - 1)) ? pMsg->valueLen : (sizeof(dn_value) - 1);
    memcpy(dn_value, pMsg->pValue, len);
    dn_value[len] = '\0';
  }
  /* Central Address Resolution */
  else if (pMsg->handle == pHdlList[GAP_CAR_HDL_IDX])
  {
    appDbHdl_t dbHdl;

    /* if there's a device record */
    if ((dbHdl = AppDbGetHdl((dmConnId_t)pMsg->hdr.param)) != APP_DB_HDL_NONE)
    {
      if ((pMsg->pValue[0] == FALSE) || (pMsg->pValue[0] == TRUE))
      {
        /* store value in device database */
        AppDbSetPeerAddrRes(dbHdl, pMsg->pValue[0]);
      }
      else
      {
        /* invalid value */
        status = ATT_ERR_RANGE;
      }

      APP_TRACE_INFO1("Central address resolution: %d", pMsg->pValue[0]);
    }
  }
  /* handle not found in list */
  else
  {
    status = ATT_ERR_NOT_FOUND;
  }

  return status;
}
/* clang-format on */
