#pragma once
/*! enumeration of client characteristic configuration descriptors */
enum
{
  BLE_GATT_SC_CCC_IDX,                    /*! GATT service, service changed characteristic */
  BLE_BATT_LVL_CCC_IDX,                   /*! Battery service, battery level characteristic */
  BLE_ESS_TEMP_CCC_IDX,                   /*! Environmental sensing service, temperature characteristic */
  BLE_ESS_HUMI_CCC_IDX,                   /*! Environmental sensing service, humidity characteristic */
  BLE_ESS_PRES_CCC_IDX,                   /*! Environmental sensing service, pressure characteristic */
  HIDAPP_MBI_CCC_HDL,                   /*! HID Boot Mouse Input characteristic */
  HIDAPP_KBI_CCC_HDL,                   /*! HID Boot Keyboard Input characteristic */
  HIDAPP_IN_KEYBOARD_CCC_HDL,           /*! HID Input Report characteristic for keyboard inputs */
  HIDAPP_IN_MOUSE_CCC_HDL,              /*! HID Input Report characteristic for mouse inputs */
  HIDAPP_IN_CONSUMER_CCC_HDL,             /*! HID Input Report characteristic for consumer control inputs */
  BLE_ESS_IAQ_CCC_IDX,                    /*! Environmental sensing service, IAQ characteristic */
  UART_TX_CH_CCC_IDX,
  BLE_NUM_CCC_IDX
};

