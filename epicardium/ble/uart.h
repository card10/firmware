#pragma once

#include "ble_api.h"

#define UART_START_HDL 0x800            /*!< \brief Service start handle. */
#define UART_END_HDL (UART_MAX_HDL - 1) /*!< \brief Service end handle. */

/*! \brief UART Service Handles */
enum { UART_SVC_HDL = UART_START_HDL, /*!< \brief UART service declaration */
       UART_RX_CH_HDL,                /*!< \brief UART rx characteristic */
       UART_RX_HDL,                   /*!< \brief UART rx value */
       UART_TX_CH_HDL,                /*!< \brief UART tx characteristic */
       UART_TX_HDL,                   /*!< \brief UART tx value */
       UART_TX_CH_CCC_HDL,            /*!< \brief UART tx CCCD */
       UART_MAX_HDL                   /*!< \brief Maximum handle. */
};

void UartProcMsg(bleMsg_t *pMsg);

