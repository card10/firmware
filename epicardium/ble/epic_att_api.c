#include "ble_api.h"
#include "epicardium.h"
#include "os/core.h"

#include "wsf_types.h"
#include "util/bstream.h"
#include "wsf_msg.h"
#include "att_api.h"
#include "wsf_buf.h"
#include "gatt/gatt_api.h"

#include "FreeRTOS.h"
#include "queue.h"

#include <stdio.h>
#include <string.h>

/* We allow up to 10 dynamically defined services */
#define ATTS_DYN_GROUP_COUNT 10
#define ATTS_DYN_START_HANDLE 0x200

static void *dyn_groups[ATTS_DYN_GROUP_COUNT] = {};
static int next_dyn_group                     = 0;
static int next_handle                        = ATTS_DYN_START_HANDLE;

void ble_epic_att_api_event(attEvt_t *att_event)
{
	if (att_event->hdr.event != ATTS_HANDLE_VALUE_CNF ||
	    (att_event->handle >= ATTS_DYN_START_HANDLE &&
	     att_event->handle < next_handle)) {
		size_t value_len = 0;
		if (att_event->hdr.event == ATTC_READ_BY_GROUP_TYPE_RSP ||
		    att_event->hdr.event == ATTC_READ_BY_TYPE_RSP ||
		    att_event->hdr.event == ATTC_FIND_INFO_RSP ||
		    att_event->hdr.event == ATTC_HANDLE_VALUE_NTF ||
		    att_event->hdr.event == ATTC_HANDLE_VALUE_IND) {
			value_len = att_event->valueLen;
		}
		attEvt_t *e = WsfBufAlloc(sizeof(*e) + value_len);

		if (e) {
			memcpy(e, att_event, sizeof(*e));
			memcpy(e + 1, att_event->pValue, value_len);
			e->pValue = (uint8_t *)(e + 1);
			ble_epic_ble_api_trigger_event(BLE_EVENT_ATT_EVENT, e);
		} else {
			LOG_WARN(
				"ble",
				"could not allocate att event of size %d",
				sizeof(*e) + att_event->valueLen
			);
		}
	}
}

void ble_epic_att_api_free_att_write_data(struct epic_att_write *w)
{
	WsfBufFree(w->buffer);
	WsfBufFree(w);
}

static uint8_t DynAttsWriteCback(
	dmConnId_t connId,
	uint16_t handle,
	uint8_t operation,
	uint16_t offset,
	uint16_t len,
	uint8_t *pValue,
	attsAttr_t *pAttr
) {
	struct epic_att_write *att_write = WsfBufAlloc(sizeof(*att_write));

	if (att_write) {
		att_write->hdr.param = connId;
		att_write->handle    = handle;
		att_write->valueLen  = len;
		att_write->operation = operation;
		att_write->offset    = offset;

		att_write->buffer = WsfBufAlloc(len);

		if (att_write->buffer) {
			memcpy(att_write->buffer, pValue, len);
			ble_epic_ble_api_trigger_event(
				BLE_EVENT_ATT_WRITE, att_write
			);
		} else {
			LOG_WARN("ble", "could allocate att write data");
			WsfBufFree(att_write);
		}
	} else {
		LOG_WARN("ble", "could allocate att write");
	}

	// TODO: handle offset != 0
	return AttsSetAttr(handle, len, pValue);
}

int epic_atts_dyn_create_service(
	const uint8_t *uuid,
	uint8_t uuid_len,
	uint16_t group_size,
	void **pSvcHandle
) {
	uint16_t start_handle = next_handle;
	uint16_t end_handle   = start_handle + group_size - 1;

	*pSvcHandle = AttsDynCreateGroup(start_handle, end_handle);
	dyn_groups[next_dyn_group++] = *pSvcHandle;

	AttsDynRegister(*pSvcHandle, NULL, DynAttsWriteCback);

	AttsDynAddAttr(
		*pSvcHandle,
		attPrimSvcUuid,
		uuid,
		uuid_len,
		uuid_len,
		0,
		ATTS_PERMIT_READ
	);
	next_handle++;

	// TODO: validate parameters and pointer and current service count
	return 0;
}

int epic_atts_dyn_add_characteristic(
	void *pSvcHandle,
	const uint8_t *uuid,
	uint8_t uuid_len,
	uint8_t flags,
	uint16_t maxLen,
	uint16_t *value_handle
) {
	uint8_t settings = ATTS_SET_VARIABLE_LEN;
	if (flags & ATT_PROP_WRITE) {
		settings |= ATTS_SET_WRITE_CBACK;
	}

	uint8_t permissions = 0;
	if (flags & ATT_PROP_READ) {
		permissions |= ATTS_PERMIT_READ;
	}
	if (flags & ATT_PROP_WRITE) {
		permissions |= ATTS_PERMIT_WRITE;
	}

	uint8_t characteristic[1 + 2 + 16] = {
		flags, UINT16_TO_BYTES(next_handle + 1)
	};
	memcpy(characteristic + 3, uuid, uuid_len);
	uint8_t characteristic_len = 1 + 2 + uuid_len;

	/* Characteristic */
	AttsDynAddAttr(
		pSvcHandle,
		attChUuid,
		characteristic,
		characteristic_len,
		characteristic_len,
		0,
		ATTS_PERMIT_READ
	);
	next_handle++;

	/* Value */
	*value_handle = next_handle;
	if ((flags & ATT_PROP_READ) == 0) {
		AttsDynAddAttrDynConst(
			pSvcHandle,
			uuid,
			uuid_len,
			NULL,
			0,
			maxLen,
			settings,
			permissions
		);
	} else {
		AttsDynAddAttrDyn(
			pSvcHandle,
			uuid,
			uuid_len,
			NULL,
			0,
			maxLen,
			settings,
			permissions
		);
	}
	next_handle++;
	return 0;
}

int epic_ble_atts_dyn_add_descriptor(
	void *pSvcHandle,
	const uint8_t *uuid,
	uint8_t uuid_len,
	uint8_t flags,
	const uint8_t *value,
	uint16_t value_len,
	uint16_t maxLen,
	uint16_t *descriptor_handle
) {
	uint8_t settings = 0;
	if (flags & ATT_PROP_WRITE) {
		settings |= ATTS_SET_WRITE_CBACK;
	}

	uint8_t permissions = 0;
	if (flags & ATT_PROP_READ) {
		permissions |= ATTS_PERMIT_READ;
	}
	if (flags & ATT_PROP_WRITE) {
		permissions |= ATTS_PERMIT_WRITE;
	}

	*descriptor_handle = next_handle;
	AttsDynAddAttrDyn(
		pSvcHandle,
		uuid,
		uuid_len,
		value,
		value_len,
		maxLen,
		settings,
		permissions
	);
	next_handle++;

	return 0;
}

int epic_atts_dyn_send_service_changed_ind(void)
{
	/* Indicate to the server that our GATT DB changed.
	 * TODO: Handling of CCCDs in pairings might still be broken:
	 * See https://git.card10.badge.events.ccc.de/card10/firmware/-/issues/197 */
	GattSendServiceChangedInd(
		DM_CONN_ID_NONE, ATT_HANDLE_START, ATT_HANDLE_MAX
	);
	return 0;
}

int epic_ble_atts_dyn_delete_groups(void)
{
	for (int i = 0; i < ATTS_DYN_GROUP_COUNT; i++) {
		if (dyn_groups[i]) {
			AttsDynDeleteGroup(dyn_groups[i]);
			dyn_groups[i] = NULL;
		}
	}
	next_dyn_group = 0;
	next_handle    = ATTS_DYN_START_HANDLE;
	return 0;
}

void ble_epic_att_api_init(void)
{
}

int epic_ble_atts_handle_value_ntf(
	uint8_t connId, uint16_t handle, uint16_t valueLen, uint8_t *pValue
) {
	if (!DmConnInUse(connId)) {
		return -EIO;
	}

	AttsHandleValueNtf(connId, handle, valueLen, pValue);
	return 0;
}

int epic_ble_atts_handle_value_ind(
	uint8_t connId, uint16_t handle, uint16_t valueLen, uint8_t *pValue
) {
	if (!DmConnInUse(connId)) {
		return -EIO;
	}

	AttsHandleValueInd(connId, handle, valueLen, pValue);
	return 0;
}

int epic_ble_atts_set_buffer(uint16_t value_handle, size_t len, bool append)
{
	return AttsDynResize(value_handle, len);
}

int epic_ble_atts_set_attr(
	uint16_t handle, const uint8_t *value, uint16_t value_len
) {
	uint8_t ret = AttsSetAttr(handle, value_len, (uint8_t *)value);
	return ret;
}

int epic_ble_attc_discover_primary_services(
	uint8_t connId, const uint8_t *uuid, uint8_t uuid_len
) {
	if (uuid_len == 0 || uuid == NULL) {
		AttcReadByGroupTypeReq(
			connId, 1, 0xFFFF, 2, (uint8_t *)attPrimSvcUuid, TRUE
		);
	} else {
		AttcFindByTypeValueReq(
			connId,
			ATT_HANDLE_START,
			ATT_HANDLE_MAX,
			ATT_UUID_PRIMARY_SERVICE,
			uuid_len,
			(uint8_t *)uuid,
			FALSE
		);
	}
	return 0;
}

int epic_ble_attc_discover_characteristics(
	uint8_t connId, uint16_t start_handle, uint16_t end_handle
) {
	AttcReadByTypeReq(
		connId,
		start_handle,
		end_handle,
		ATT_16_UUID_LEN,
		(uint8_t *)attChUuid,
		TRUE
	);
	return 0;
}

int epic_ble_attc_discover_descriptors(
	uint8_t connId, uint16_t start_handle, uint16_t end_handle
) {
	AttcFindInfoReq(connId, start_handle, end_handle, TRUE);
	return 0;
}

int epic_ble_attc_read(uint8_t connId, uint16_t value_handle)
{
	AttcReadReq(connId, value_handle);
	return 0;
}

int epic_ble_attc_write_no_rsp(
	uint8_t connId,
	uint16_t value_handle,
	const uint8_t *value,
	uint16_t value_len
) {
	AttcWriteCmd(connId, value_handle, value_len, (uint8_t *)value);
	return 0;
}
int epic_ble_attc_write(
	uint8_t connId,
	uint16_t value_handle,
	const uint8_t *value,
	uint16_t value_len
) {
	AttcWriteReq(connId, value_handle, value_len, (uint8_t *)value);
	return 0;
}
