/*************************************************************************************************/
/*!
 *  \file
 *
 *  \brief  Application framework device database example, using simple RAM-based storage.
 *
 *  Copyright (c) 2011-2018 Arm Ltd. All Rights Reserved.
 *  ARM Ltd. confidential and proprietary.
 *
 *  IMPORTANT.  Your use of this file is governed by a Software License Agreement
 *  ("Agreement") that must be accepted in order to download or otherwise receive a
 *  copy of this file.  You may not use or copy this file for any purpose other than
 *  as described in the Agreement.  If you do not agree to all of the terms of the
 *  Agreement do not use this file and delete all copies in your possession or control;
 *  if you do not have a copy of the Agreement, you must contact ARM Ltd. prior
 *  to any use, copying or further distribution of this software.
 */
/*************************************************************************************************/

/* card10:
 * copied from: lib/sdk/Libraries/BTLE/stack/ble-profiles/sources/apps/app/common/app_db.c
 *
 * Reason: we need to implement persistent storage for bondings
 */
/* clang-format off */
/* clang-formet turned off for easier diffing against orginal file */
#include "wsf_types.h"
#include "wsf_assert.h"
#include "util/bda.h"
#include "app_api.h"
#include "app_main.h"
#include "app_db.h"
#include "app_cfg.h"

#include "epicardium.h"
#include "os/core.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "mxc_sys.h"
#include "wdt.h"
#include <string.h>
#include <stdio.h>

/**************************************************************************************************
  Macros
**************************************************************************************************/

/* App DB NVM record parameter indicies from upstream:
 * https://github.com/packetcraft-inc/stacks/blob/master/ble-profiles/sources/af/common/app_db.c#L46
 */
#define APP_DB_NVM_IN_USE_ID                  0
#define APP_DB_NVM_PEER_ADDR_ID               1
#define APP_DB_NVM_ADDR_TYPE_ID               2
#define APP_DB_NVM_PEER_IRK_ID                3
#define APP_DB_NVM_PEER_CSRK_ID               4
#define APP_DB_NVM_KV_MASK_ID                 5
#define APP_DB_NVM_VALID_ID                   6
#define APP_DB_NVM_PEER_RAPO_ID               7
#define APP_DB_NVM_LOCAL_LTK_ID               8
#define APP_DB_NVM_LOCAL_SEC_LVL_ID           9
#define APP_DB_NVM_PEER_ADDR_RES_ID           10
#define APP_DB_NVM_PEER_LTK_ID                11
#define APP_DB_NVM_PEER_SEC_LVL_ID            12
#define APP_DB_NVM_CCC_TBL_ID                 13
#define APP_DB_NVM_PEER_SIGN_CTR_ID           14
#define APP_DB_NVM_CAS_ID                     15
#define APP_DB_NVM_CSF_ID                     16
#define APP_DB_NVM_CACHE_HASH_ID              17
#define APP_DB_NVM_HASH_ID                    18
#define APP_DB_NVM_HDL_LIST_ID                19
#define APP_DB_NVM_DISC_STATUS_ID             20

#define APP_DB_NVM_SEQUENCE_NUMBER_ID         100

/**************************************************************************************************
  Data Types
**************************************************************************************************/

/*! Database record */
typedef struct
{
  /*! Common for all roles */
  bdAddr_t    peerAddr;                     /*! Peer address */
  uint8_t     addrType;                     /*! Peer address type */
  dmSecIrk_t  peerIrk;                      /*! Peer IRK */
  dmSecCsrk_t peerCsrk;                     /*! Peer CSRK */
  uint8_t     keyValidMask;                 /*! Valid keys in this record */
  bool_t      inUse;                        /*! TRUE if record in use */
  bool_t      valid;                        /*! TRUE if record is valid */
  bool_t      peerAddedToRl;                /*! TRUE if peer device's been added to resolving list */
  bool_t      peerRpao;                     /*! TRUE if RPA Only attribute's present on peer device */

  /*! For slave local device */
  dmSecLtk_t  localLtk;                     /*! Local LTK */
  uint8_t     localLtkSecLevel;             /*! Local LTK security level */
  bool_t      peerAddrRes;                  /*! TRUE if address resolution's supported on peer device (master) */

  /*! For master local device */
  dmSecLtk_t  peerLtk;                      /*! Peer LTK */
  uint8_t     peerLtkSecLevel;              /*! Peer LTK security level */

  /*! for ATT server local device */
  uint16_t    cccTbl[APP_DB_NUM_CCCD];      /*! Client characteristic configuration descriptors */
  uint32_t    peerSignCounter;              /*! Peer Sign Counter */

  /*! for ATT client */
  uint16_t    hdlList[APP_DB_HDL_LIST_LEN]; /*! Cached handle list */
  uint8_t     discStatus;                   /*! Service discovery and configuration status */

  uint32_t    sequenceNumber;
} appDbRec_t;


/**************************************************************************************************
  Local Variables
**************************************************************************************************/

/*! Database */
static appDbRec_t records[APP_DB_NUM_RECS];

/* clang-format on */
/* Translate a pointer to a record into the filename to be used for it. */
static int record_to_filename(appDbRec_t *record, char *buf, size_t buf_size)
{
	int id  = record - records;
	int ret = snprintf(buf, buf_size, "pairings/pairing%d.bin", id + 1);
	if (ret >= (int)buf_size) {
		ret = -1;
	}
	return ret;
}

static appDbRec_t *record_with_highest_seq_number()
{
	appDbRec_t *r = &records[0];
	for (int i = 0; i < APP_DB_NUM_RECS; i++) {
		if (records[i].sequenceNumber > r->sequenceNumber) {
			r = &records[i];
		}
	}
	return r;
}

static appDbRec_t *record_with_lowest_seq_number()
{
	appDbRec_t *r = &records[0];
	for (int i = 0; i < APP_DB_NUM_RECS; i++) {
		if (records[i].sequenceNumber < r->sequenceNumber) {
			r = &records[i];
		}
	}
	return r;
}

/* Write a TLV to a file. */
static int write_tlv(int fd, uint32_t t, uint32_t l, void *v)
{
	int ret;

	ret = epic_file_write(fd, &t, sizeof(t));
	if (ret != sizeof(t))
		return ret;

	ret = epic_file_write(fd, &l, sizeof(l));
	if (ret != sizeof(l))
		return ret;

	ret = epic_file_write(fd, v, l);
	if (ret != (int)l)
		return ret;

	return 0;
}

/* Read a TLV from a file.
 *
 * Super naive implementation assuming that the next TLV is
 * the expected one. */
static int read_tlv(int fd, uint32_t t, uint32_t l, void *v)
{
	int ret;

	uint32_t t_r;
	ret = epic_file_read(fd, &t_r, sizeof(t_r));
	if (ret != sizeof(t))
		return ret;
	if (t != t_r)
		return -ENOENT;

	uint32_t l_r;
	ret = epic_file_read(fd, &l_r, sizeof(l_r));
	if (ret != sizeof(l_r))
		return ret;
	if (l_r > l)
		return -EINVAL;

	memset(v, 0, l);

	ret = epic_file_read(fd, v, l_r);
	if (ret != (int)l_r)
		return ret;

	return 0;
}

static int write_bond_to_file(appDbRec_t *r, char *filename)
{
	if (!r->inUse) {
		return -EINVAL;
	}

	int fd = epic_file_open(filename, "w");
	int ret;
	if (fd < 0) {
		return fd;
	}

	static const uint8_t version = 1;
	ret = epic_file_write(fd, &version, sizeof(version));
	if (ret != sizeof(version))
		goto out;

#define write_element(t, x)                                                    \
	if ((ret = write_tlv(fd, t, sizeof(r->x), &r->x)))                     \
		goto out;

	write_element(APP_DB_NVM_PEER_ADDR_ID, peerAddr);
	write_element(APP_DB_NVM_ADDR_TYPE_ID, addrType);
	write_element(APP_DB_NVM_PEER_IRK_ID, peerIrk);
	write_element(APP_DB_NVM_PEER_CSRK_ID, peerCsrk);
	write_element(APP_DB_NVM_KV_MASK_ID, keyValidMask);
	/* peerAddedToRl not persisted by upstream */
	/* write_element(, peerAddedToRl); */
	write_element(APP_DB_NVM_PEER_RAPO_ID, peerRpao);
	write_element(APP_DB_NVM_LOCAL_LTK_ID, localLtk);
	write_element(APP_DB_NVM_LOCAL_SEC_LVL_ID, localLtkSecLevel);
	write_element(APP_DB_NVM_PEER_ADDR_RES_ID, peerAddrRes);
	write_element(APP_DB_NVM_PEER_LTK_ID, peerLtk);
	write_element(APP_DB_NVM_PEER_SEC_LVL_ID, peerLtkSecLevel);
	write_element(APP_DB_NVM_CCC_TBL_ID, cccTbl);
	write_element(APP_DB_NVM_PEER_SIGN_CTR_ID, peerSignCounter);
	write_element(APP_DB_NVM_HDL_LIST_ID, hdlList);
	write_element(APP_DB_NVM_DISC_STATUS_ID, discStatus);
	write_element(APP_DB_NVM_SEQUENCE_NUMBER_ID, sequenceNumber);
	write_element(APP_DB_NVM_VALID_ID, valid);

#undef write_element

out:
	epic_file_close(fd);
	return ret;
}

static int read_bond_from_file(appDbRec_t *r, char *filename)
{
	int fd = epic_file_open(filename, "r");
	if (fd < 0) {
		return fd;
	}

	uint8_t version;
	int ret = epic_file_read(fd, &version, sizeof(version));
	if (ret != sizeof(version)) {
		goto out;
	}
	if (version != 1) {
		ret = -EINVAL;
		goto out;
	}

#define read_element(t, x)                                                     \
	if ((ret = read_tlv(fd, t, sizeof(r->x), &r->x)))                      \
		goto out;

	read_element(APP_DB_NVM_PEER_ADDR_ID, peerAddr);
	read_element(APP_DB_NVM_ADDR_TYPE_ID, addrType);
	read_element(APP_DB_NVM_PEER_IRK_ID, peerIrk);
	read_element(APP_DB_NVM_PEER_CSRK_ID, peerCsrk);
	read_element(APP_DB_NVM_KV_MASK_ID, keyValidMask);
	/* peerAddedToRl not persisted by upstream */
	/* read_element(, peerAddedToRl); */
	read_element(APP_DB_NVM_PEER_RAPO_ID, peerRpao);
	read_element(APP_DB_NVM_LOCAL_LTK_ID, localLtk);
	read_element(APP_DB_NVM_LOCAL_SEC_LVL_ID, localLtkSecLevel);
	read_element(APP_DB_NVM_PEER_ADDR_RES_ID, peerAddrRes);
	read_element(APP_DB_NVM_PEER_LTK_ID, peerLtk);
	read_element(APP_DB_NVM_PEER_SEC_LVL_ID, peerLtkSecLevel);
	read_element(APP_DB_NVM_CCC_TBL_ID, cccTbl);
	read_element(APP_DB_NVM_PEER_SIGN_CTR_ID, peerSignCounter);
	read_element(APP_DB_NVM_HDL_LIST_ID, hdlList);
	read_element(APP_DB_NVM_DISC_STATUS_ID, discStatus);
	read_element(APP_DB_NVM_SEQUENCE_NUMBER_ID, sequenceNumber);
	read_element(APP_DB_NVM_VALID_ID, valid);

#undef read_element

	r->inUse = true;
out:
	epic_file_close(fd);
	return ret;
}

static int delete_bond(char *filename)
{
	return epic_file_unlink(filename);
}

/*************************************************************************************************/
/*!
 *  \brief  Initialize the device database.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbInit(void)
{
	memset(&records, 0, sizeof(records));

	char filename[32];
	for (int i = 0; i < APP_DB_NUM_RECS; i++) {
		record_to_filename(&records[i], filename, sizeof(filename));
		int ret = read_bond_from_file(&records[i], filename);
		if (ret < 0) {
			if (ret != -ENOENT) {
				LOG_WARN(
					"bondings",
					"Reading pairing '%s' failed: %d",
					filename,
					ret
				);
			}
			memset(&records[i], 0, sizeof(records[i]));
		}
	}
}

/* clang-format off */
/*************************************************************************************************/
/*!
 *  \brief  Create a new device database record.
 *
 *  \param  addrType  Address type.
 *  \param  pAddr     Peer device address.
 *
 *  \return Database record handle.
 */
/*************************************************************************************************/
appDbHdl_t AppDbNewRecord(uint8_t addrType, uint8_t *pAddr)
{
  appDbRec_t  *pRec = records;
  uint8_t     i;

  /* find a free record */
  for (i = APP_DB_NUM_RECS; i > 0; i--, pRec++)
  {
    if (!pRec->inUse)
    {
      break;
    }
  }

  /* if all records were allocated */
  if (i == 0)
  {
    /* overwrite the oldest record */
    pRec = record_with_lowest_seq_number();
  }

  /* initialize record */
  memset(pRec, 0, sizeof(appDbRec_t));
  pRec->inUse = TRUE;
  pRec->addrType = addrType;
  BdaCpy(pRec->peerAddr, pAddr);
  pRec->peerAddedToRl = FALSE;
  pRec->peerRpao = FALSE;
  pRec->sequenceNumber = record_with_highest_seq_number()->sequenceNumber + 1;

  return (appDbHdl_t) pRec;
}

/*************************************************************************************************/
/*!
*  \brief  Get the next database record for a given record. For the first record, the function
*          should be called with 'hdl' set to 'APP_DB_HDL_NONE'.
*
*  \param  hdl  Database record handle.
*
*  \return Next record handle found. APP_DB_HDL_NONE, otherwise.
*/
/*************************************************************************************************/
appDbHdl_t AppDbGetNextRecord(appDbHdl_t hdl)
{
  appDbRec_t  *pRec;

  /* if first record is requested */
  if (hdl == APP_DB_HDL_NONE)
  {
    pRec = records;
  }
  /* if valid record passed in */
  else if (AppDbRecordInUse(hdl))
  {
    pRec = (appDbRec_t *)hdl;
    pRec++;
  }
  /* invalid record passed in */
  else
  {
    return APP_DB_HDL_NONE;
  }

  /* look for next valid record */
  while (pRec < &records[APP_DB_NUM_RECS])
  {
    /* if record is in use */
    if (pRec->inUse && pRec->valid)
    {
      /* record found */
      return (appDbHdl_t)pRec;
    }

    /* look for next record */
    pRec++;
  }

  /* end of records */
  return APP_DB_HDL_NONE;
}

/*************************************************************************************************/
/*!
 *  \brief  Delete a new device database record.
 *
 *  \param  hdl       Database record handle.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbDeleteRecord(appDbHdl_t hdl)
{
  ((appDbRec_t *) hdl)->inUse = FALSE;
  char filename[32];
  record_to_filename((appDbRec_t *) hdl, filename, sizeof(filename));
  delete_bond(filename);
}

/*************************************************************************************************/
/*!
 *  \brief  Validate a new device database record.  This function is called when pairing is
 *          successful and the devices are bonded.
 *
 *  \param  hdl       Database record handle.
 *  \param  keyMask   Bitmask of keys to validate.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbValidateRecord(appDbHdl_t hdl, uint8_t keyMask)
{
  ((appDbRec_t *) hdl)->valid = TRUE;
  ((appDbRec_t *) hdl)->keyValidMask = keyMask;
}

/*************************************************************************************************/
/*!
 *  \brief  Check if a record has been validated.  If it has not, delete it.  This function
 *          is typically called when the connection is closed.
 *
 *  \param  hdl       Database record handle.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbCheckValidRecord(appDbHdl_t hdl)
{
  if (((appDbRec_t *) hdl)->valid == FALSE)
  {
    AppDbDeleteRecord(hdl);
  }
}

/*************************************************************************************************/
/*!
*  \brief  Check if a database record is in use.

*  \param  hdl       Database record handle.
*
*  \return TURE if record in use. FALSE, otherwise.
*/
/*************************************************************************************************/
bool_t AppDbRecordInUse(appDbHdl_t hdl)
{
  appDbRec_t  *pRec = records;
  uint8_t     i;

  /* see if record is in database record list */
  for (i = APP_DB_NUM_RECS; i > 0; i--, pRec++)
  {
    if (pRec->inUse && pRec->valid && (pRec == ((appDbRec_t *)hdl)))
    {
      return TRUE;
    }
  }

  return FALSE;
}

/*************************************************************************************************/
/*!
 *  \brief  Check if there is a stored bond with any device.
 *
 *  \param  hdl       Database record handle.
 *
 *  \return TRUE if a bonded device is found, FALSE otherwise.
 */
/*************************************************************************************************/
bool_t AppDbCheckBonded(void)
{
  appDbRec_t  *pRec = records;
  uint8_t     i;

  /* find a record */
  for (i = APP_DB_NUM_RECS; i > 0; i--, pRec++)
  {
    if (pRec->inUse)
    {
      return TRUE;
    }
  }

  return FALSE;
}

/*************************************************************************************************/
/*!
 *  \brief  Delete all database records.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbDeleteAllRecords(void)
{
  appDbRec_t  *pRec = records;
  uint8_t     i;

  /* set in use to false for all records */
  for (i = APP_DB_NUM_RECS; i > 0; i--, pRec++)
  {
    pRec->inUse = FALSE;
  }
}

/*************************************************************************************************/
/*!
 *  \brief  Find a device database record by peer address.
 *
 *  \param  addrType  Address type.
 *  \param  pAddr     Peer device address.
 *
 *  \return Database record handle or APP_DB_HDL_NONE if not found.
 */
/*************************************************************************************************/
appDbHdl_t AppDbFindByAddr(uint8_t addrType, uint8_t *pAddr)
{
  appDbRec_t  *pRec = records;
  uint8_t     peerAddrType = DmHostAddrType(addrType);
  uint8_t     i;

  /* find matching record */
  for (i = APP_DB_NUM_RECS; i > 0; i--, pRec++)
  {
    if (pRec->inUse && (pRec->addrType == peerAddrType) && BdaCmp(pRec->peerAddr, pAddr))
    {
      return (appDbHdl_t) pRec;
    }
  }

  return APP_DB_HDL_NONE;
}

/*************************************************************************************************/
/*!
 *  \brief  Find a device database record by data in an LTK request.
 *
 *  \param  encDiversifier  Encryption diversifier associated with key.
 *  \param  pRandNum        Pointer to random number associated with key.
 *
 *  \return Database record handle or APP_DB_HDL_NONE if not found.
 */
/*************************************************************************************************/
appDbHdl_t AppDbFindByLtkReq(uint16_t encDiversifier, uint8_t *pRandNum)
{
  appDbRec_t  *pRec = records;
  uint8_t     i;

  /* find matching record */
  for (i = APP_DB_NUM_RECS; i > 0; i--, pRec++)
  {
    if (pRec->inUse && (pRec->localLtk.ediv == encDiversifier) &&
        (memcmp(pRec->localLtk.rand, pRandNum, SMP_RAND8_LEN) == 0))
    {
      return (appDbHdl_t) pRec;
    }
  }

  return APP_DB_HDL_NONE;
}

/*************************************************************************************************/
/*!
 *  \brief  Get a key from a device database record.
 *
 *  \param  hdl       Database record handle.
 *  \param  type      Type of key to get.
 *  \param  pSecLevel If the key is valid, the security level of the key.
 *
 *  \return Pointer to key if key is valid or NULL if not valid.
 */
/*************************************************************************************************/
dmSecKey_t *AppDbGetKey(appDbHdl_t hdl, uint8_t type, uint8_t *pSecLevel)
{
  dmSecKey_t *pKey = NULL;

  /* if key valid */
  if ((type & ((appDbRec_t *) hdl)->keyValidMask) != 0)
  {
    switch(type)
    {
      case DM_KEY_LOCAL_LTK:
        *pSecLevel = ((appDbRec_t *) hdl)->localLtkSecLevel;
        pKey = (dmSecKey_t *) &((appDbRec_t *) hdl)->localLtk;
        break;

      case DM_KEY_PEER_LTK:
        *pSecLevel = ((appDbRec_t *) hdl)->peerLtkSecLevel;
        pKey = (dmSecKey_t *) &((appDbRec_t *) hdl)->peerLtk;
        break;

      case DM_KEY_IRK:
        pKey = (dmSecKey_t *)&((appDbRec_t *)hdl)->peerIrk;
        break;

      case DM_KEY_CSRK:
        pKey = (dmSecKey_t *)&((appDbRec_t *)hdl)->peerCsrk;
        break;

      default:
        break;
    }
  }

  return pKey;
}

/*************************************************************************************************/
/*!
 *  \brief  Set a key in a device database record.
 *
 *  \param  hdl       Database record handle.
 *  \param  pKey      Key data.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbSetKey(appDbHdl_t hdl, dmSecKeyIndEvt_t *pKey)
{
  switch(pKey->type)
  {
    case DM_KEY_LOCAL_LTK:
      ((appDbRec_t *) hdl)->localLtkSecLevel = pKey->secLevel;
      ((appDbRec_t *) hdl)->localLtk = pKey->keyData.ltk;
      break;

    case DM_KEY_PEER_LTK:
      ((appDbRec_t *) hdl)->peerLtkSecLevel = pKey->secLevel;
      ((appDbRec_t *) hdl)->peerLtk = pKey->keyData.ltk;
      break;

    case DM_KEY_IRK:
      ((appDbRec_t *)hdl)->peerIrk = pKey->keyData.irk;

      /* make sure peer record is stored using its identity address */
      ((appDbRec_t *)hdl)->addrType = pKey->keyData.irk.addrType;
      BdaCpy(((appDbRec_t *)hdl)->peerAddr, pKey->keyData.irk.bdAddr);
      break;

    case DM_KEY_CSRK:
      ((appDbRec_t *)hdl)->peerCsrk = pKey->keyData.csrk;

      /* sign counter must be initialized to zero when CSRK is generated */
      ((appDbRec_t *)hdl)->peerSignCounter = 0;
      break;

    default:
      break;
  }
}

/*************************************************************************************************/
/*!
 *  \brief  Get the client characteristic configuration descriptor table.
 *
 *  \param  hdl       Database record handle.
 *
 *  \return Pointer to client characteristic configuration descriptor table.
 */
/*************************************************************************************************/
uint16_t *AppDbGetCccTbl(appDbHdl_t hdl)
{
  return ((appDbRec_t *) hdl)->cccTbl;
}

/*************************************************************************************************/
/*!
 *  \brief  Set a value in the client characteristic configuration table.
 *
 *  \param  hdl       Database record handle.
 *  \param  idx       Table index.
 *  \param  value     client characteristic configuration value.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbSetCccTblValue(appDbHdl_t hdl, uint16_t idx, uint16_t value)
{
  WSF_ASSERT(idx < APP_DB_NUM_CCCD);

  ((appDbRec_t *) hdl)->cccTbl[idx] = value;
}

/*************************************************************************************************/
/*!
 *  \brief  Get the discovery status.
 *
 *  \param  hdl       Database record handle.
 *
 *  \return Discovery status.
 */
/*************************************************************************************************/
uint8_t AppDbGetDiscStatus(appDbHdl_t hdl)
{
  return ((appDbRec_t *) hdl)->discStatus;
}

/*************************************************************************************************/
/*!
 *  \brief  Set the discovery status.
 *
 *  \param  hdl       Database record handle.
 *  \param  state     Discovery status.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbSetDiscStatus(appDbHdl_t hdl, uint8_t status)
{
  ((appDbRec_t *) hdl)->discStatus = status;
}

/*************************************************************************************************/
/*!
 *  \brief  Get the cached handle list.
 *
 *  \param  hdl       Database record handle.
 *
 *  \return Pointer to handle list.
 */
/*************************************************************************************************/
uint16_t *AppDbGetHdlList(appDbHdl_t hdl)
{
  return ((appDbRec_t *) hdl)->hdlList;
}

/*************************************************************************************************/
/*!
 *  \brief  Set the cached handle list.
 *
 *  \param  hdl       Database record handle.
 *  \param  pHdlList  Pointer to handle list.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbSetHdlList(appDbHdl_t hdl, uint16_t *pHdlList)
{
  memcpy(((appDbRec_t *) hdl)->hdlList, pHdlList, sizeof(((appDbRec_t *) hdl)->hdlList));
}

/*************************************************************************************************/
/*!
 *  \brief  Get address resolution attribute value read from a peer device.
 *
 *  \param  hdl        Database record handle.
 *
 *  \return TRUE if address resolution is supported in peer device. FALSE, otherwise.
 */
/*************************************************************************************************/
bool_t AppDbGetPeerAddrRes(appDbHdl_t hdl)
{
  return ((appDbRec_t *)hdl)->peerAddrRes;
}

/*************************************************************************************************/
/*!
 *  \brief  Set address resolution attribute value for a peer device.
 *
 *  \param  hdl        Database record handle.
 *  \param  addrRes    Address resolution attribue value.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbSetPeerAddrRes(appDbHdl_t hdl, uint8_t addrRes)
{
  ((appDbRec_t *)hdl)->peerAddrRes = addrRes;
}

/*************************************************************************************************/
/*!
 *  \brief  Get sign counter for a peer device.
 *
 *  \param  hdl        Database record handle.
 *
 *  \return Sign counter for peer device.
 */
/*************************************************************************************************/
uint32_t AppDbGetPeerSignCounter(appDbHdl_t hdl)
{
  return ((appDbRec_t *)hdl)->peerSignCounter;
}

/*************************************************************************************************/
/*!
 *  \brief  Set sign counter for a peer device.
 *
 *  \param  hdl          Database record handle.
 *  \param  signCounter  Sign counter for peer device.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbSetPeerSignCounter(appDbHdl_t hdl, uint32_t signCounter)
{
  if(((appDbRec_t *)hdl)->peerSignCounter != signCounter) {
    ((appDbRec_t *)hdl)->peerSignCounter = signCounter;
  }
}

/*************************************************************************************************/
/*!
 *  \brief  Get the peer device added to resolving list flag value.
 *
 *  \param  hdl        Database record handle.
 *
 *  \return TRUE if peer device's been added to resolving list. FALSE, otherwise.
 */
/*************************************************************************************************/
bool_t AppDbGetPeerAddedToRl(appDbHdl_t hdl)
{
  return ((appDbRec_t *)hdl)->peerAddedToRl;
}

/*************************************************************************************************/
/*!
 *  \brief  Set the peer device added to resolving list flag to a given value.
 *
 *  \param  hdl           Database record handle.
 *  \param  peerAddedToRl Peer device added to resolving list flag value.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbSetPeerAddedToRl(appDbHdl_t hdl, bool_t peerAddedToRl)
{
  ((appDbRec_t *)hdl)->peerAddedToRl = peerAddedToRl;
}

/*************************************************************************************************/
/*!
 *  \brief  Get the resolvable private address only attribute flag for a given peer device.
 *
 *  \param  hdl        Database record handle.
 *
 *  \return TRUE if RPA Only attribute is present on peer device. FALSE, otherwise.
 */
/*************************************************************************************************/
bool_t AppDbGetPeerRpao(appDbHdl_t hdl)
{
  return ((appDbRec_t *)hdl)->peerRpao;
}

/*************************************************************************************************/
/*!
 *  \brief  Set the resolvable private address only attribute flag for a given peer device.
 *
 *  \param  hdl        Database record handle.
 *  \param  peerRpao   Resolvable private address only attribute flag value.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbSetPeerRpao(appDbHdl_t hdl, bool_t peerRpao)
{
  ((appDbRec_t *)hdl)->peerRpao = peerRpao;
}

/*************************************************************************************************/
/*!
 *  \brief  Store the client characteristic configuration table for a device record in NVM.
 *
 *  \param  hdl       Database record handle.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbNvmStoreCccTbl(appDbHdl_t hdl)
{
  /* We take a short cut and simply write the whole file again. */
  AppDbNvmStoreBond(hdl);
}

/*************************************************************************************************/
/*!
 *  \brief  Store bonding information for device record in NVM.
 *
 *  \param  hdl       Database record handle.
 *
 *  \return None.
 */
/*************************************************************************************************/
void AppDbNvmStoreBond(appDbHdl_t hdl)
{
  appDbRec_t  *pRec = (appDbRec_t *) hdl;

  if (pRec->inUse && pRec->valid) {
    char filename[32];
    record_to_filename(pRec, filename, sizeof(filename));
    /* Directory might exist already. Call will fail silently in that case. */
    epic_file_mkdir("pairings");
    int ret = write_bond_to_file(pRec, filename);
    if(ret < 0) {
		LOG_WARN(
			"bondings",
			"Writing pairing '%s' failed: %d",
			filename,
			ret
		);
	}
  }
}
/* clang-format on */

int AppDbGetPairingName(appDbHdl_t hdl, char *buf, size_t buf_size)
{
	appDbRec_t *pRec = (appDbRec_t *)hdl;
	return record_to_filename(pRec, buf, buf_size);
}
