/*
 * Based on ble-profiles/sources/apps/hidapp/hidapp_main.c
 */
#include "cccd.h"
#include "hid.h"

#include "wsf_types.h"
#include "dm_api.h"
#include "att_api.h"
#include "svc_hid.h"
#include "hid/hid_api.h"

#include "os/core.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>

/*! HidApp Report Map (Descriptor) */
/* clang-format off */

/* Based on https://github.com/adafruit/Adafruit_CircuitPython_BLE/blob/master/adafruit_ble/services/standard/hid.py */
const uint8_t hidReportMap[] =
{
	0x05, 0x01,  /* Usage Page (Generic Desktop Ctrls) */
	0x09, 0x06,  /* Usage (Keyboard) */
	0xA1, 0x01,  /* Collection (Application) */
	0x85, HIDAPP_KEYBOARD_REPORT_ID,  /*   Report ID (1) */
	0x05, 0x07,  /*   Usage Page (Kbrd/Keypad) */
	0x19, 0xE0,  /*   Usage Minimum (\xE0) */
	0x29, 0xE7,  /*   Usage Maximum (\xE7) */
	0x15, 0x00,  /*   Logical Minimum (0) */
	0x25, 0x01,  /*   Logical Maximum (1) */
	0x75, 0x01,  /*   Report Size (1) */
	0x95, 0x08,  /*   Report Count (8) */
	0x81, 0x02,  /*   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position) */
	0x81, 0x01,  /*   Input (Const,Array,Abs,No Wrap,Linear,Preferred State,No Null Position) */
	0x19, 0x00,  /*   Usage Minimum (\x00) */
	0x29, 0x89,  /*   Usage Maximum (\x89) */
	0x15, 0x00,  /*   Logical Minimum (0) */
	0x25, 0x89,  /*   Logical Maximum (137) */
	0x75, 0x08,  /*   Report Size (8) */
	0x95, 0x06,  /*   Report Count (6) */
	0x81, 0x00,  /*   Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position) */
	0x05, 0x08,  /*   Usage Page (LEDs) */
	0x19, 0x01,  /*   Usage Minimum (Num Lock) */
	0x29, 0x05,  /*   Usage Maximum (Kana) */
	0x15, 0x00,  /*   Logical Minimum (0) */
	0x25, 0x01,  /*   Logical Maximum (1) */
	0x75, 0x01,  /*   Report Size (1) */
	0x95, 0x05,  /*   Report Count (5) */
	0x91, 0x02,  /*   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile) */
	0x95, 0x03,  /*   Report Count (3) */
	0x91, 0x01,  /*   Output (Const,Array,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile) */
	0xC0,        /* End Collection */
	0x05, 0x01,  /* Usage Page (Generic Desktop Ctrls) */
	0x09, 0x02,  /* Usage (Mouse) */
	0xA1, 0x01,  /* Collection (Application) */
	0x09, 0x01,  /*   Usage (Pointer) */
	0xA1, 0x00,  /*   Collection (Physical) */
	0x85, HIDAPP_MOUSE_REPORT_ID,  /*     Report ID (2) */
	0x05, 0x09,  /*     Usage Page (Button) */
	0x19, 0x01,  /*     Usage Minimum (\x01) */
	0x29, 0x05,  /*     Usage Maximum (\x05) */
	0x15, 0x00,  /*     Logical Minimum (0) */
	0x25, 0x01,  /*     Logical Maximum (1) */
	0x95, 0x05,  /*     Report Count (5) */
	0x75, 0x01,  /*     Report Size (1) */
	0x81, 0x02,  /*     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position) */
	0x95, 0x01,  /*     Report Count (1) */
	0x75, 0x03,  /*     Report Size (3) */
	0x81, 0x01,  /*     Input (Const,Array,Abs,No Wrap,Linear,Preferred State,No Null Position) */
	0x05, 0x01,  /*     Usage Page (Generic Desktop Ctrls) */
	0x09, 0x30,  /*     Usage (X) */
	0x09, 0x31,  /*     Usage (Y) */
	0x15, 0x81,  /*     Logical Minimum (-127) */
	0x25, 0x7F,  /*     Logical Maximum (127) */
	0x75, 0x08,  /*     Report Size (8) */
	0x95, 0x02,  /*     Report Count (2) */
	0x81, 0x06,  /*     Input (Data,Var,Rel,No Wrap,Linear,Preferred State,No Null Position) */
	0x09, 0x38,  /*     Usage (Wheel) */
	0x15, 0x81,  /*     Logical Minimum (-127) */
	0x25, 0x7F,  /*     Logical Maximum (127) */
	0x75, 0x08,  /*     Report Size (8) */
	0x95, 0x01,  /*     Report Count (1) */
	0x81, 0x06,  /*     Input (Data,Var,Rel,No Wrap,Linear,Preferred State,No Null Position) */
	0xC0,        /*   End Collection */
	0xC0,        /* End Collection */
	0x05, 0x0C,        /* Usage Page (Consumer) */
	0x09, 0x01,        /* Usage (Consumer Control) */
	0xA1, 0x01,        /* Collection (Application) */
	0x85, HIDAPP_CONSUMER_REPORT_ID,        /*   Report ID (3) */
	0x75, 0x10,        /*   Report Size (16) */
	0x95, 0x01,        /*   Report Count (1) */
	0x15, 0x01,        /*   Logical Minimum (1) */
	0x26, 0x8C, 0x02,  /*   Logical Maximum (652) */
	0x19, 0x01,        /*   Usage Minimum (Consumer Control) */
	0x2A, 0x8C, 0x02,  /*   Usage Maximum (AC Send) */
	0x81, 0x00,        /*   Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position) */
	0xC0,              /* End Collection */
#if 0
	0x05, 0x01,        /* Usage Page (Generic Desktop Ctrls) */
	0x09, 0x05,        /* Usage (Game Pad) */
	0xA1, 0x01,        /* Collection (Application) */
	0x85, 0x05,        /*   Report ID (5) */
	0x05, 0x09,        /*   Usage Page (Button) */
	0x19, 0x01,        /*   Usage Minimum (\x01) */
	0x29, 0x10,        /*   Usage Maximum (\x10) */
	0x15, 0x00,        /*   Logical Minimum (0) */
	0x25, 0x01,        /*   Logical Maximum (1) */
	0x75, 0x01,        /*   Report Size (1) */
	0x95, 0x10,        /*   Report Count (16) */
	0x81, 0x02,        /*   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position) */
	0x05, 0x01,        /*   Usage Page (Generic Desktop Ctrls) */
	0x15, 0x81,        /*   Logical Minimum (-127) */
	0x25, 0x7F,        /*   Logical Maximum (127) */
	0x09, 0x30,        /*   Usage (X) */
	0x09, 0x31,        /*   Usage (Y) */
	0x09, 0x32,        /*   Usage (Z) */
	0x09, 0x35,        /*   Usage (Rz) */
	0x75, 0x08,        /*   Report Size (8) */
	0x95, 0x04,        /*   Report Count (4) */
	0x81, 0x02,        /*   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position) */
	0xC0,              /* End Collection */
#endif
};
/* clang-format on */

const uint16_t hidReportMapLen = sizeof(hidReportMap);

/*! HID Report Type/ID and attribute handle map */
/* clang-format off */
static const hidReportIdMap_t hidAppReportIdSet[] =
{
  /* type                       ID                            handle */
  {HID_REPORT_TYPE_INPUT,       HIDAPP_KEYBOARD_REPORT_ID,    HID_INPUT_REPORT_1_HDL},     /* Keyboard Input Report */
  {HID_REPORT_TYPE_OUTPUT,      HIDAPP_KEYBOARD_REPORT_ID,    HID_OUTPUT_REPORT_HDL},      /* Keyboard Output Report */
  {HID_REPORT_TYPE_FEATURE,     HIDAPP_KEYBOARD_REPORT_ID,    HID_FEATURE_REPORT_HDL},     /* Keyboard Feature Report */
  {HID_REPORT_TYPE_INPUT,       HIDAPP_MOUSE_REPORT_ID,       HID_INPUT_REPORT_2_HDL},     /* Mouse Input Report */
  {HID_REPORT_TYPE_INPUT,       HIDAPP_CONSUMER_REPORT_ID,    HID_INPUT_REPORT_3_HDL},     /* Consumer Control Input Report */
  {HID_REPORT_TYPE_INPUT,       HID_KEYBOARD_BOOT_ID,         HID_KEYBOARD_BOOT_IN_HDL},   /* Boot Keyboard Input Report */
  {HID_REPORT_TYPE_OUTPUT,      HID_KEYBOARD_BOOT_ID,         HID_KEYBOARD_BOOT_OUT_HDL},  /* Boot Keyboard Output Report */
  {HID_REPORT_TYPE_INPUT,       HID_MOUSE_BOOT_ID,            HID_MOUSE_BOOT_IN_HDL},      /* Boot Mouse Input Report */
};
/* clang-format on */

void hidAppOutputCback(
	dmConnId_t connId, uint8_t id, uint16_t len, uint8_t *pReport
);
void hidAppFeatureCback(
	dmConnId_t connId, uint8_t id, uint16_t len, uint8_t *pReport
);
void hidAppInfoCback(dmConnId_t connId, uint8_t type, uint8_t value);

/*! HID Profile Configuration */
/* clang-format off */
static const hidConfig_t hidAppHidConfig =
{
  (hidReportIdMap_t*) hidAppReportIdSet,                  /* Report ID to Attribute Handle map */
  sizeof(hidAppReportIdSet)/sizeof(hidReportIdMap_t),     /* Size of Report ID to Attribute Handle map */
  &hidAppOutputCback,                                     /* Output Report Callback */
  &hidAppFeatureCback,                                    /* Feature Report Callback */
  &hidAppInfoCback                                        /* Info Callback */
};
/* clang-format on */

static void hidAppReportInit(void)
{
	uint8_t iKeyboardBuffer[HIDAPP_KEYBOARD_INPUT_REPORT_LEN];
	uint8_t iMouseBuffer[HIDAPP_MOUSE_INPUT_REPORT_LEN];
	uint8_t iConsumerBuffer[HIDAPP_CONSUMER_INPUT_REPORT_LEN];
	uint8_t oBuffer[HIDAPP_OUTPUT_REPORT_LEN];
	uint8_t fBuffer[HIDAPP_FEATURE_REPORT_LEN];

	/* Keyboard Input report */
	memset(iKeyboardBuffer, 0, HIDAPP_KEYBOARD_INPUT_REPORT_LEN);
	AttsSetAttr(
		HID_INPUT_REPORT_1_HDL,
		HIDAPP_KEYBOARD_INPUT_REPORT_LEN,
		iKeyboardBuffer
	);

	/* Mouse Input report */
	memset(iMouseBuffer, 0, HIDAPP_MOUSE_INPUT_REPORT_LEN);
	AttsSetAttr(
		HID_INPUT_REPORT_2_HDL,
		HIDAPP_MOUSE_INPUT_REPORT_LEN,
		iMouseBuffer
	);

	/* Consumer Control Input report */
	memset(iConsumerBuffer, 0, HIDAPP_CONSUMER_INPUT_REPORT_LEN);
	AttsSetAttr(
		HID_INPUT_REPORT_3_HDL,
		HIDAPP_CONSUMER_INPUT_REPORT_LEN,
		iConsumerBuffer
	);

	/* Output report */
	memset(oBuffer, 0, HIDAPP_OUTPUT_REPORT_LEN);
	AttsSetAttr(HID_OUTPUT_REPORT_HDL, HIDAPP_OUTPUT_REPORT_LEN, oBuffer);

	/* Feature report */
	memset(fBuffer, 0, HIDAPP_FEATURE_REPORT_LEN);
	AttsSetAttr(HID_FEATURE_REPORT_HDL, HIDAPP_FEATURE_REPORT_LEN, fBuffer);
}

void hid_work_init(void);
void hid_init(void)
{
	SvcHidAddGroup();
	SvcHidRegister(HidAttsWriteCback, NULL);

	/* Initialize the HID profile */
	HidInit(&hidAppHidConfig);

	/* Initialize the report attributes */
	hidAppReportInit();

	hid_work_init();
}
