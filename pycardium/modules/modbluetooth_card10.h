#pragma once

/*
 * card10 related BLE defines. They are taken from the cordio (not card10!)
 * BLE stack which is used by the card10.
 */

#define ATT_SUCCESS 0x00         /*!< \brief Operation successful */
#define ATT_ERR_HANDLE 0x01      /*!< \brief Invalid handle */
#define ATT_ERR_READ 0x02        /*!< \brief Read not permitted */
#define ATT_ERR_WRITE 0x03       /*!< \brief Write not permitted */
#define ATT_ERR_INVALID_PDU 0x04 /*!< \brief Invalid pdu */
#define ATT_ERR_AUTH 0x05        /*!< \brief Insufficient authentication */
#define ATT_ERR_NOT_SUP 0x06     /*!< \brief Request not supported */
#define ATT_ERR_OFFSET 0x07      /*!< \brief Invalid offset */
#define ATT_ERR_AUTHOR 0x08      /*!< \brief Insufficient authorization */
#define ATT_ERR_QUEUE_FULL 0x09  /*!< \brief Prepare queue full */
#define ATT_ERR_NOT_FOUND 0x0A   /*!< \brief Attribute not found */
#define ATT_ERR_NOT_LONG 0x0B    /*!< \brief Attribute not long */
#define ATT_ERR_KEY_SIZE 0x0C    /*!< \brief Insufficient encryption key size */
#define ATT_ERR_LENGTH 0x0D      /*!< \brief Invalid attribute value length */
#define ATT_ERR_UNLIKELY 0x0E    /*!< \brief Other unlikely error */
#define ATT_ERR_ENC 0x0F         /*!< \brief Insufficient encryption */
#define ATT_ERR_GROUP_TYPE 0x10  /*!< \brief Unsupported group type */
#define ATT_ERR_RESOURCES 0x11   /*!< \brief Insufficient resources */
#define ATT_ERR_DATABASE_OUT_OF_SYNC                                           \
	0x12 /*!< \brief Client out of synch with database */
#define ATT_ERR_VALUE_NOT_ALLOWED 0x13 /*!< \brief Value not allowed */
#define ATT_ERR_WRITE_REJ 0xFC         /*!< \brief Write request rejected */
#define ATT_ERR_CCCD 0xFD              /*!< \brief CCCD improperly configured */
#define ATT_ERR_IN_PROGRESS 0xFE /*!< \brief Procedure already in progress */
#define ATT_ERR_RANGE 0xFF       /*!< \brief Value out of range */
/**@}*/

/** \name Proprietary Internal Error Codes
 * These codes may be sent to application but are not present in any ATT PDU.
 */
/**@{*/
#define ATT_ERR_MEMORY 0x70      /*!< \brief Out of memory */
#define ATT_ERR_TIMEOUT 0x71     /*!< \brief Transaction timeout */
#define ATT_ERR_OVERFLOW 0x72    /*!< \brief Transaction overflow */
#define ATT_ERR_INVALID_RSP 0x73 /*!< \brief Invalid response PDU */
#define ATT_ERR_CANCELLED 0x74   /*!< \brief Request cancelled */
#define ATT_ERR_UNDEFINED 0x75   /*!< \brief Other undefined error */
#define ATT_ERR_REQ_NOT_FOUND                                                  \
	0x76 /*!< \brief Required characteristic not found */
#define ATT_ERR_MTU_EXCEEDED                                                   \
	0x77 /*!< \brief Attribute PDU length exceeded MTU size */
#define ATT_CONTINUING 0x78 /*!< \brief Procedure continuing */
#define ATT_RSP_PENDING                                                        \
	0x79 /*!< \brief Responsed delayed pending higher layer */

#define DM_CBACK_START 0x20 /*!< \brief DM callback event starting value */

/*! \brief DM callback events */
enum { DM_RESET_CMPL_IND = DM_CBACK_START, /*!< \brief Reset complete */
       DM_ADV_START_IND,                   /*!< \brief Advertising started */
       DM_ADV_STOP_IND,                    /*!< \brief Advertising stopped */
       DM_ADV_NEW_ADDR_IND, /*!< \brief New resolvable address has been generated */
       DM_SCAN_START_IND,    /*!< \brief Scanning started */
       DM_SCAN_STOP_IND,     /*!< \brief Scanning stopped */
       DM_SCAN_REPORT_IND,   /*!< \brief Scan data received from peer device */
       DM_CONN_OPEN_IND,     /*!< \brief Connection opened */
       DM_CONN_CLOSE_IND,    /*!< \brief Connection closed */
       DM_CONN_UPDATE_IND,   /*!< \brief Connection update complete */
       DM_SEC_PAIR_CMPL_IND, /*!< \brief Pairing completed successfully */
       DM_SEC_PAIR_FAIL_IND, /*!< \brief Pairing failed or other security failure */
       DM_SEC_ENCRYPT_IND,      /*!< \brief Connection encrypted */
       DM_SEC_ENCRYPT_FAIL_IND, /*!< \brief Encryption failed */
       DM_SEC_AUTH_REQ_IND, /*!< \brief PIN or OOB data requested for pairing */
       DM_SEC_KEY_IND,      /*!< \brief Security key indication */
       DM_SEC_LTK_REQ_IND,  /*!< \brief LTK requested for encyption */
       DM_SEC_PAIR_IND,     /*!< \brief Incoming pairing request from master */
       DM_SEC_SLAVE_REQ_IND, /*!< \brief Incoming security request from slave */
       DM_SEC_CALC_OOB_IND, /*!< \brief Result of OOB Confirm Calculation Generation */
       DM_SEC_ECC_KEY_IND, /*!< \brief Result of ECC Key Generation */
       DM_SEC_COMPARE_IND, /*!< \brief Result of Just Works/Numeric Comparison Compare Value Calculation */
       DM_SEC_KEYPRESS_IND, /*!< \brief Keypress indication from peer in passkey security */
       DM_PRIV_RESOLVED_ADDR_IND, /*!< \brief Private address resolved */
       DM_PRIV_GENERATE_ADDR_IND, /*!< \brief Private resolvable address generated */
       DM_CONN_READ_RSSI_IND,           /*!< \brief Connection RSSI read */
       DM_PRIV_ADD_DEV_TO_RES_LIST_IND, /*!< \brief Device added to resolving list */
       DM_PRIV_REM_DEV_FROM_RES_LIST_IND, /*!< \brief Device removed from resolving list */
       DM_PRIV_CLEAR_RES_LIST_IND,     /*!< \brief Resolving list cleared */
       DM_PRIV_READ_PEER_RES_ADDR_IND, /*!< \brief Peer resolving address read */
       DM_PRIV_READ_LOCAL_RES_ADDR_IND, /*!< \brief Local resolving address read */
       DM_PRIV_SET_ADDR_RES_ENABLE_IND, /*!< \brief Address resolving enable set */
       DM_REM_CONN_PARAM_REQ_IND, /*!< \brief Remote connection parameter requested */
       DM_CONN_DATA_LEN_CHANGE_IND, /*!< \brief Data length changed */
       DM_CONN_WRITE_AUTH_TO_IND, /*!< \brief Write authenticated payload complete */
       DM_CONN_AUTH_TO_EXPIRED_IND, /*!< \brief Authenticated payload timeout expired */
       DM_PHY_READ_IND,             /*!< \brief Read PHY */
       DM_PHY_SET_DEF_IND,          /*!< \brief Set default PHY */
       DM_PHY_UPDATE_IND,           /*!< \brief PHY update */
       DM_ADV_SET_START_IND,  /*!< \brief Advertising set(s) started */
       DM_ADV_SET_STOP_IND,   /*!< \brief Advertising set(s) stopped */
       DM_SCAN_REQ_RCVD_IND,  /*!< \brief Scan request received */
       DM_EXT_SCAN_START_IND, /*!< \brief Extended scanning started */
       DM_EXT_SCAN_STOP_IND,  /*!< \brief Extended scanning stopped */
       DM_EXT_SCAN_REPORT_IND, /*!< \brief Extended scan data received from peer device */
       DM_PER_ADV_SET_START_IND, /*!< \brief Periodic advertising set started */
       DM_PER_ADV_SET_STOP_IND,  /*!< \brief Periodic advertising set stopped */
       DM_PER_ADV_SYNC_EST_IND, /*!< \brief Periodic advertising sync established */
       DM_PER_ADV_SYNC_EST_FAIL_IND, /*!< \brief Periodic advertising sync establishment failed */
       DM_PER_ADV_SYNC_LOST_IND, /*!< \brief Periodic advertising sync lost */
       DM_PER_ADV_SYNC_TRSF_EST_IND, /*!< \brief Periodic advertising sync transfer established */
       DM_PER_ADV_SYNC_TRSF_EST_FAIL_IND, /*!< \brief Periodic advertising sync transfer establishment failed */
       DM_PER_ADV_SYNC_TRSF_IND, /*!< \brief Periodic advertising sync info transferred */
       DM_PER_ADV_SET_INFO_TRSF_IND, /*!< \brief Periodic advertising set sync info transferred */
       DM_PER_ADV_REPORT_IND, /*!< \brief Periodic advertising data received from peer device */
       DM_REMOTE_FEATURES_IND, /*!< \brief Remote features from peer device */
       DM_READ_REMOTE_VER_INFO_IND, /*!< \brief Remote LL version information read */
       DM_CONN_IQ_REPORT_IND, /*!< \brief IQ samples from CTE of received packet from peer device */
       DM_CTE_REQ_FAIL_IND,             /*!< \brief CTE request failed */
       DM_CONN_CTE_RX_SAMPLE_START_IND, /*!< \brief Sampling received CTE started */
       DM_CONN_CTE_RX_SAMPLE_STOP_IND, /*!< \brief Sampling received CTE stopped */
       DM_CONN_CTE_TX_CFG_IND, /*!< \brief Connection CTE transmit parameters configured */
       DM_CONN_CTE_REQ_START_IND, /*!< \brief Initiating connection CTE request started */
       DM_CONN_CTE_REQ_STOP_IND, /*!< \brief Initiating connection CTE request stopped */
       DM_CONN_CTE_RSP_START_IND, /*!< \brief Responding to connection CTE request started */
       DM_CONN_CTE_RSP_STOP_IND, /*!< \brief Responding to connection CTE request stopped */
       DM_READ_ANTENNA_INFO_IND, /*!< \brief Antenna information read */
       DM_L2C_CMD_REJ_IND,       /*!< \brief L2CAP Command Reject */
       DM_ERROR_IND,             /*!< \brief General error */
       DM_HW_ERROR_IND,          /*!< \brief Hardware error */
       DM_VENDOR_SPEC_IND        /*!< \brief Vendor specific event */
};

/** \name ATT Callback Events
 * Events related to ATT transactions.
 */
/**@{*/
#define ATT_CBACK_START                  0x02    /*!< \brief ATT callback event starting value */

/*! \brief ATT client callback events */
enum                                        /*!< \brief Internal note: event values match method values */
{
  ATTC_FIND_INFO_RSP = ATT_CBACK_START,     /*!< \brief Find information response */
  ATTC_FIND_BY_TYPE_VALUE_RSP,              /*!< \brief Find by type value response */
  ATTC_READ_BY_TYPE_RSP,                    /*!< \brief Read by type value response */
  ATTC_READ_RSP,                            /*!< \brief Read response */
  ATTC_READ_LONG_RSP,                       /*!< \brief Read long response */
  ATTC_READ_MULTIPLE_RSP,                   /*!< \brief Read multiple response */
  ATTC_READ_BY_GROUP_TYPE_RSP,              /*!< \brief Read group type response */
  ATTC_WRITE_RSP,                           /*!< \brief Write response */
  ATTC_WRITE_CMD_RSP,                       /*!< \brief Write command response */
  ATTC_PREPARE_WRITE_RSP,                   /*!< \brief Prepare write response */
  ATTC_EXECUTE_WRITE_RSP,                   /*!< \brief Execute write response */
  ATTC_HANDLE_VALUE_NTF,                    /*!< \brief Handle value notification */
  ATTC_HANDLE_VALUE_IND,                    /*!< \brief Handle value indication */
  /* ATT server callback events */
  ATTS_HANDLE_VALUE_CNF,                    /*!< \brief Handle value confirmation */
  ATTS_CCC_STATE_IND,                       /*!< \brief Client chracteristic configuration state change */
  ATTS_DB_HASH_CALC_CMPL_IND,               /*!< \brief Database hash calculation complete */
  /* ATT common callback events */
  ATT_MTU_UPDATE_IND                        /*!< \brief Negotiated MTU value */
};

/*! \brief ATT callback events */
#define ATT_CBACK_END                    ATT_MTU_UPDATE_IND  /*!< \brief ATT callback event ending value */
/**@}*/



