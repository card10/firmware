#include "epicardium.h"

#include "RD117_MBED/algorithm/algorithm.h"
#include "py/builtin.h"
#include "py/obj.h"
#include "py/runtime.h"

struct spo2_memory {
	uint32_t pun_ir_buffer[500];
	uint32_t pun_red_buffer[500];
};

static mp_obj_t mp_maxim_rd177(mp_obj_t ir, mp_obj_t red)
{
	int32_t n_ir_buffer_length;

	int32_t pn_spo2       = 0;
	int8_t pch_spo2_valid = 0;
	int32_t pn_heart_rate = 0;
	int8_t pch_hr_valid   = 0;

	size_t ir_len;
	mp_obj_t *ir_elem;
	mp_obj_get_array(ir, &ir_len, &ir_elem);

	if (ir_len < 100 || ir_len > 500) {
		nlr_raise(mp_obj_new_exception_msg_varg(
			&mp_type_TypeError,
			"rd177 needs a tuple of length 100 to 500 (%d given)",
			ir_len)
		);
	}

	size_t red_len;
	mp_obj_t *red_elem;
	mp_obj_get_array(red, &red_len, &red_elem);

	if (red_len != ir_len) {
		nlr_raise(mp_obj_new_exception_msg_varg(
			&mp_type_TypeError,
			"Length of ir and red data needs to be equal")
		);
	}

	struct spo2_memory *m =
		(struct spo2_memory *)MP_STATE_PORT(spo2_memory);
	if (!m) {
		/* Will raise an exception if out of memory: */
		m = m_malloc(sizeof(struct spo2_memory));
		MP_STATE_PORT(spo2_memory) = m;
	}

	n_ir_buffer_length = ir_len;
	for (size_t i = 0; i < ir_len; i++) {
		m->pun_ir_buffer[i]  = mp_obj_get_int(ir_elem[i]);
		m->pun_red_buffer[i] = mp_obj_get_int(red_elem[i]);
	}

	maxim_heart_rate_and_oxygen_saturation(
		m->pun_ir_buffer,
		n_ir_buffer_length,
		m->pun_red_buffer,
		&pn_spo2,
		&pch_spo2_valid,
		&pn_heart_rate,
		&pch_hr_valid
	);

	mp_obj_t spo2       = mp_obj_new_int(pn_spo2);
	mp_obj_t hr         = mp_obj_new_int(pn_heart_rate);
	mp_obj_t spo2_valid = mp_obj_new_int(pch_spo2_valid);
	mp_obj_t hr_valid   = mp_obj_new_int(pch_hr_valid);

	mp_obj_t tup[] = { spo2, spo2_valid, hr, hr_valid };

	return mp_obj_new_tuple(4, tup);
}
static MP_DEFINE_CONST_FUN_OBJ_2(maxim_rd117, mp_maxim_rd177);

static const mp_rom_map_elem_t spo2_algo_module_globals_table[] = {
	{ MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_spo2_algo) },
	{ MP_ROM_QSTR(MP_QSTR_maxim_rd117), MP_ROM_PTR(&maxim_rd117) },
};
static MP_DEFINE_CONST_DICT(
	spo2_algo_module_globals, spo2_algo_module_globals_table
);

// Define module object.
const mp_obj_module_t spo2_algo_module = {
	.base    = { &mp_type_module },
	.globals = (mp_obj_dict_t *)&spo2_algo_module_globals,
};

/* Register the module to make it available in Python */
/* clang-format off */
MP_REGISTER_MODULE(MP_QSTR_spo2_algo, spo2_algo_module, MODULE_SPO2_ALGO_ENABLED);
