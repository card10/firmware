#include "py/obj.h"
#include "py/objlist.h"
#include "py/runtime.h"
#include "py/builtin.h"
#include "api/common.h"
#include "mphalport.h"

#include "epicardium.h"

STATIC mp_obj_t mp_max86150_enable_sensor(size_t n_args, const mp_obj_t *args)
{
	struct max86150_sensor_config cfg = { 0 };
	cfg.sample_buffer_len             = mp_obj_get_int(args[0]);
	cfg.ppg_sample_rate               = mp_obj_get_int(args[1]);

	int stream_id = epic_max86150_enable_sensor(&cfg, sizeof(cfg));

	if (stream_id < 0) {
		mp_raise_OSError(-stream_id);
	}

	return MP_OBJ_NEW_SMALL_INT(stream_id);
}

STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	mp_max86150_enable_sensor_obj, 2, 2, mp_max86150_enable_sensor
);

STATIC mp_obj_t mp_max86150_read_sensor(mp_obj_t stream_id_in)
{
	// do not use too big buf
	// 256*12 is close to stack size of 4096, reduced to 64
	struct max86150_sensor_data buf[64];
	int stream_id = mp_obj_get_int(stream_id_in);

	int n = epic_stream_read(stream_id, buf, sizeof(buf));

	mp_obj_list_t *list = mp_obj_new_list(0, NULL);
	for (int i = 0; i < n; i++) {
		mp_obj_t tuple[3];

		tuple[0] = mp_obj_new_int(buf[i].red);
		tuple[1] = mp_obj_new_int(buf[i].ir);
		tuple[2] = mp_obj_new_int(buf[i].ecg);

		mp_obj_list_append(list, mp_obj_new_tuple(3, tuple));
	}

	return MP_OBJ_FROM_PTR(list);
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(
	mp_max86150_read_sensor_obj, mp_max86150_read_sensor
);

STATIC mp_obj_t mp_max86150_disable_sensor(void)
{
	int ret = epic_max86150_disable_sensor();

	if (ret != 0) {
		mp_raise_OSError(-ret);
	}

	return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_0(
	mp_max86150_disable_sensor_obj, mp_max86150_disable_sensor
);

STATIC const mp_rom_map_elem_t max86150_module_globals_table[] = {
	{ MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_sys_max86150) },
	{ MP_ROM_QSTR(MP_QSTR_enable_sensor),
	  MP_ROM_PTR(&mp_max86150_enable_sensor_obj) },
	{ MP_ROM_QSTR(MP_QSTR_read_sensor),
	  MP_ROM_PTR(&mp_max86150_read_sensor_obj) },
	{ MP_ROM_QSTR(MP_QSTR_disable_sensor),
	  MP_ROM_PTR(&mp_max86150_disable_sensor_obj) },
};
STATIC MP_DEFINE_CONST_DICT(
	max86150_module_globals, max86150_module_globals_table
);

const mp_obj_module_t max86150_module = {
	.base    = { &mp_type_module },
	.globals = (mp_obj_dict_t *)&max86150_module_globals,
};

/* clang-format off */
/* Register the module to make it available in Python */
MP_REGISTER_MODULE(MP_QSTR_sys_max86150, max86150_module, MODULE_MAX86150_ENABLED);
