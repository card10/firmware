import sys_ble_hid
import time


class Report:
    """
    The Report class provides an interface to the Adafruit CircuitPython HID library
    (https://github.com/adafruit/Adafruit_CircuitPython_HID/).

    ``ble_hid.devices`` exposes a list of reports for use with the CircuitPython HID
    classes. You usually do not have to interact with a report yourself but you can make
    use of its ``send_report`` method to send raw HID reports to the host.

    **Example using Adafruit CircuitPython HID library**:

    .. code-block:: python

        import ble_hid
        from adafruit_hid.mouse import Mouse

        m = Mouse(ble_hid.devices)
        m.click(Mouse.MIDDLE_BUTTON)

    **Example using raw non blocking access**:
    .. code-block:: python

        import ble_hid
        report = ble_hid.Report(report_id=3, blocking=False) # Consumer Control
        report.send_report(b'\\xe9\\x00') # 0x00E9: Volume Increase
        report.send_report(b'\\x00\\x00') # 0x0000: Release button


    .. versionadded:: 1.17
    """

    def __init__(self, report_id, blocking=False, usage_page=None, usage=None):
        """
        Initializes a report.

        All parameters are available as properties of the resulting object and can be modified
        during runtime.

        :param report_id: The id of the report. Currently supported: 1: Keyboard, 2: Mouse, 3: Consumer control
        :param blocking: If True :py:func`send_report()` will try sending the report until
                         there is space in the queue (unless the host is not connected).
        :param usage_page: Used by Adafruit CircuitPython HID library to identify report
        :param usage: Used by Adafruit CircuitPython HID library to identify report
        """

        self.report_id = report_id
        self.usage_page = usage_page
        self.usage = usage
        self.blocking = True

    def send_report(self, data):
        """
        Tries to send a report to the host.

        :param data: The data to be sent. Must not exceed the configured length of the report.
        :rtype: bool
        :returns: `True` if the report was sent, `False` if the report was queued for sending.
        :raises OSError: if there is no connection to a host or BLE HID is not enabled.
        :raises MemoryError: if there was no space in the queue (only raised if ``blocking`` was set to `False`).
        """

        if self.blocking:
            # Loop until we are able to place the report in the queue
            # Forward all other exceptions to the caller
            while True:
                try:
                    return sys_ble_hid.send_report(self.report_id, data)
                    break
                except MemoryError:
                    time.sleep(0.1)
        else:
            # Forward all exceptions to the caller
            return sys_ble_hid.send_report(self.report_id, data)


# Reports as defined in the HID report map in epicardium/ble/hid.c
devices = [
    Report(report_id=1, blocking=True, usage_page=0x01, usage=0x06),
    Report(report_id=2, blocking=True, usage_page=0x01, usage=0x02),
    Report(report_id=3, blocking=True, usage_page=0x0C, usage=0x01),
]
