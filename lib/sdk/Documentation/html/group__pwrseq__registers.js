var group__pwrseq__registers =
[
    [ "Register Offsets", "group__PWRSEQ__Register__Offsets.html", "group__PWRSEQ__Register__Offsets" ],
    [ "PWRSEQ_LPCN", "group__PWRSEQ__LPCN.html", "group__PWRSEQ__LPCN" ],
    [ "PWRSEQ_LPWKST0", "group__PWRSEQ__LPWKST0.html", "group__PWRSEQ__LPWKST0" ],
    [ "PWRSEQ_LPWKEN0", "group__PWRSEQ__LPWKEN0.html", "group__PWRSEQ__LPWKEN0" ],
    [ "PWRSEQ_LPWKST1", "group__PWRSEQ__LPWKST1.html", "group__PWRSEQ__LPWKST1" ],
    [ "PWRSEQ_LPWKEN1", "group__PWRSEQ__LPWKEN1.html", "group__PWRSEQ__LPWKEN1" ],
    [ "PWRSEQ_LPPWST", "group__PWRSEQ__LPPWST.html", "group__PWRSEQ__LPPWST" ],
    [ "PWRSEQ_LPPWEN", "group__PWRSEQ__LPPWEN.html", "group__PWRSEQ__LPPWEN" ],
    [ "PWRSEQ_LPMEMSD", "group__PWRSEQ__LPMEMSD.html", "group__PWRSEQ__LPMEMSD" ],
    [ "PWRSEQ_LPVDDPD", "group__PWRSEQ__LPVDDPD.html", "group__PWRSEQ__LPVDDPD" ],
    [ "mxc_pwrseq_regs_t", "structmxc__pwrseq__regs__t.html", [
      [ "lpcn", "structmxc__pwrseq__regs__t.html#acda7cefaab59b1351d543490c7de08fd", null ],
      [ "lpwkst0", "structmxc__pwrseq__regs__t.html#a363b89a70bd871d3b80e0fa321ed770d", null ],
      [ "lpwken0", "structmxc__pwrseq__regs__t.html#aea953e86a35e4980504849547ce92a6a", null ],
      [ "lpwkst1", "structmxc__pwrseq__regs__t.html#af8c6160d69b43c2cb7e61167b83945cf", null ],
      [ "lpwken1", "structmxc__pwrseq__regs__t.html#a537b6832bfc78f6b1a2fe2e08a2869f4", null ],
      [ "rsv_0x14_0x2f", "structmxc__pwrseq__regs__t.html#ab1819eafea71a5fae3b7612e98806265", null ],
      [ "lppwst", "structmxc__pwrseq__regs__t.html#a3b45e8fe1081cbbe7119e15a31607a59", null ],
      [ "lppwen", "structmxc__pwrseq__regs__t.html#a01e74384b07af3d4cc25d9df3119fdde", null ],
      [ "rsv_0x38_0x3f", "structmxc__pwrseq__regs__t.html#a7bc615764cb890796b046bf04a0a534e", null ],
      [ "lpmemsd", "structmxc__pwrseq__regs__t.html#a04874b86ca61cac518d93937357222ea", null ],
      [ "lpvddpd", "structmxc__pwrseq__regs__t.html#a8061fe974b25823102de7c85f444ceb2", null ],
      [ "buretvec", "structmxc__pwrseq__regs__t.html#a275e49a4b3f5f23ec82436ac339972fe", null ],
      [ "buaod", "structmxc__pwrseq__regs__t.html#a2afa0f0e932e56ca6e98bd3dda5e7eaf", null ]
    ] ]
];