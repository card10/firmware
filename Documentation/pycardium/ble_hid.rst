``ble_hid`` - BLE HID
=====================
The ``ble_hid`` module provides access to the BLE Human Interface Device functionality.

.. note::
    Make sure to enable the BLE HID functionality in :ref:`card10_cfg` and reboot your card10
    if you want to use BLE HID.

    Also make sure that the ``adafruit_hid`` folder from the card10 release archive is placed
    on the file system of your card10.


.. warning::
    At least Ubuntu Linux will keep auto connecting to BLE HID devices once they are
    paired to the host computer. If you want to connect your card10 to a phone again,
    you might have to temporarily turn off Bluetooth on your computer.

An example application can be found in the preload directory (named ``HID Demo``). It provides
examples how to use the card10 as keyboard, mouse or volume control.

Please refer to the Adafruit CircuitPython HID library for examples how to use the HID service.
The card10 implements the same HID descriptors as the Adafruit CircuitPython BLE library and
should be compatible with all uses of the Adafruit CircuitPython HID library.

**Example emulating a keyboard**:

Adapted from https://github.com/adafruit/Adafruit_Learning_System_Guides/blob/master/CPB_Keybutton_BLE/cpb_keybutton_ble.py

A more complete version of this example can be found in the HID Demo app on your card10.

.. code-block:: python

    import ble_hid
    from adafruit_hid.keyboard import Keyboard
    from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
    from adafruit_hid.keycode import Keycode

    k = Keyboard(ble_hid.devices)
    kl = KeyboardLayoutUS(k)

    k.send(Keycode.BACKSPACE)

    # use keyboard_layout for words
    kl.write("Demo with a long text to show how fast a card10 can type!")

    # add shift modifier
    k.send(Keycode.SHIFT, Keycode.F19)


**Example emulating a mouse**:

.. code-block:: python

    import ble_hid
    import bhi160
    import buttons
    from adafruit_hid.mouse import Mouse

    m = Mouse(ble_hid.devices)

    def send_report(samples):
        if len(samples) > 0:
            x = -int(samples[0].z)
            y = -int(samples[0].y)
            m.move(x, y)

    sensor = bhi160.BHI160Orientation(sample_rate=10, callback=send_report)

    b_old = buttons.read()
    while True:
        b_new = buttons.read()
        if not b_old == b_new:
            print(b_new)
            b_old = b_new
            if b_new == buttons.TOP_RIGHT:
                m.click(Mouse.MIDDLE_BUTTON)
            elif b_new == buttons.BOTTOM_RIGHT:
                m.click(Mouse.RIGHT_BUTTON)
            elif b_new == buttons.BOTTOM_LEFT:
                m.click(Mouse.LEFT_BUTTON)

.. note::
    Make sure to catch ``OSError`` exceptions in real applications. The exception will be thrown if
    there is connection to the host (or if it is lost) and you want to send an event.


.. automodule:: ble_hid
   :members:
