.. _ESS:

Environmental Sensing Service
=============================

The Environmental Sensing Service (ESS) implements access to
the BME680 environmental sensor of the card10.

It provides:

- Temperature
- Relative humidity
- Pressure

If :ref:`bsec_api` is enabled the following additional estimates are available:

 - Indoor air quality (IAQ estimate
 - Equivalent CO2 (eCO2) estimate

Please refer to :py:mod:`bme680` for more information about BSEC.


If notifcations are enabled a measurement of all values is performed every 3 seconds. For each measurement a notification is sent for the characteristics which have notifications enabled.


A measurement can also be triggered by reading from a characteristic. A measurement takes roughly 200 ms. A notifciation will be sent to all characteristics which have notifications enabled except the one which was used to trigger the measurement.

.. note::
   If :ref:`bsec_api` is enabled, reading a characteristic will not trigger a new measurement.

.. note::
    This service will be available in version v1.17.


BLE Service
-----------

- Service

  UUID: ``181A``

- Temperature characteristic:

  UUID: ``2A6E``
  read and notify

- Humidity characteristic:

  UUID: ``2A6F``
  read and notify

- Pressure characteristic:

  UUID: ``2A6D``
  read and notify

- Indoor air quality (IAQ) characteristic:

  UUID: ``422302f1-2342-2342-2342-234223422342``
  read and notify

Temperature characteristic
--------------------------

- 16 bit little endian value representing the measured temperature.

- Unit: 0.01 deg C


Humidity characteristic
-----------------------

- 16 bit little endian value representing the measured relative humidity.

- Unit: 0.01%

Pressure characteristic
-----------------------

- 32 bit little endian value representing the measured pressure.

- Unit: 0.1 Pa (0.001 hPa)

Indoor air quality (IAQ) characteristic
---------------------------------------

Data format:

======== =========================== ===========================
Byte 0   Bytes 1-2                   Bytes 3-4
-------- --------------------------- ---------------------------
Accuracy IAQ (16-bit little endian)  eCO2 (16-bit little endian)
======== =========================== ===========================


Units:

- Accuracy and IAQ units: See :ref:`bsec_api` API description
- CO2 unit: [ppm]
