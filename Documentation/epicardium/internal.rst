.. _epicardium_internal_apis:

Epicardium Internal APIs
========================

Core OS APIs
------------
.. c:autodoc:: epicardium/os/core.h
